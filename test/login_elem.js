
var AngularPage = function () {
  browser.get('https://app.p3fy.com/');
};

 AngularPage.prototype = Object.create({}, {
    email: { get: function () { return element(by.model('credentials.email')); }},
    password: { get: function () { return element(by.model('credentials.password')); }},
    typeEmail: { value: function (keys) { return this.email.sendKeys(keys); }},
    typePassword: { value: function (pwd) {
     return this.password.sendKeys(pwd); 
 }},
    loginButton: { get: function(){return element(by.buttonText('Sign In'));}},
    emailText:   { get: function () { return this.email.getText() }},
    passText:    { get: function () { return this.password.getText() }},
    login: { value: function () {
    this.loginButton.click();
  }}
});

module.exports = AngularPage;