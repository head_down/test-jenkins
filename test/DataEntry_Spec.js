var Login_Page = require('./Login_Page.js');

var DataEntry_Page = require('./DataEntry_Page.js');

var loginData = require('./Login_Page_Data.json');

var testData = require('./DataEntry_Data.json');

var EC = protractor.ExpectedCondxitions;

describe('Category: Profiles, Sub-Category: Validation Test', function() {

    beforeAll(function(done) {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;

        done();
    });

    beforeEach(function() {

        DataEntry_Page.navigateToLoginPageProd();

        Login_Page.enterEmailAddress(loginData[0].emailID);

        Login_Page.enterPassword(loginData[0].password);

        Login_Page.clickOnSignIn();

        DataEntry_Page.clickOnProfileName();

        DataEntry_Page.clickOnAddEntry();

    });

    it('TC_13: Should enter name for validation test succesfully', function() {

        DataEntry_Page.enterValidationTestName(testData[0].name);

        expect(DataEntry_Page.getEnteredValidationTestName()).toEqual(testData[0].name);
    });

    afterEach(function() {

        //clear session
        browser.executeScript('window.sessionStorage.clear();');

        //clear local storage
        browser.executeScript('window.localStorage.clear();');

    });

});