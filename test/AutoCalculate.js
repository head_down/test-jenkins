var locator=require("./Locators.js").config
var EC = protractor.ExpectedConditions;
describe('AutoCalculate Testing:', function() {
	browser.sleep(3000);
	it('should able to navigating to AutoCalculate data entry page', function() {
		var task=element.all(by.css('a[href="#/task/114190"]')).get(1);
		browser.executeScript('window.scrollTo(0,1000);').then(function () {
			task.click();
		});
		browser.sleep(5000);
		
	});

	it('we are in data entry page, should able to enter name:', function() {    
		element(by.xpath(locator.TaskAddEntry)).click();
	    element(by.name('name')).sendKeys('Auto Calculate Testing').then(function(text) {
	    	console.log('Name entered sucessfully');
	    });
	});
	it('Should able to enter boys_count in first element:', function() {    
	  element(by.name('boys_count')).sendKeys('500').then(function(text) {
	    console.log('Boys count entered sucessfully');
	  });
	});
	it('Should able to enter girls_count in second element:', function() {    
	  element(by.name('girls_count')).sendKeys('400').then(function(text) {
	    console.log('Girls count entered sucessfully');
	  });
	});
	it('Should able to enter boys_recount value:', function() {    
	  element(by.name('boys_recount')).sendKeys('500').then(function(text) {
	    console.log('Boys count entered sucessfully');
	  });
	});
    it("should able to select school location from drop down.", function () {
         element.all(by.name('school_loc')).get(0).click().then(function() {
           console.log("School location as 'Goa'is selected sucessfully");
         });
    });
	it('Should able to enter school establishment date sucessfully:', function() {    
	  element(by.name('estd_date')).sendKeys('09/06/2000').then(function(text) {
	    console.log('Establishment date entered sucessfully');
	  });
	});
	it("should able to select refered profile from drop down.", function () {
         element.all(by.name('pro_ref')).get(0).click().then(function() {
           console.log("Profile referenced element is selected sucessfully");
         });
    });
	it('Should able to enter feedback for school sucessfully:', function() {    
	  element(by.name('feedback')).sendKeys('Good School').then(function(text) {
	    console.log('Feedback entered sucessfully');
	  });
	});
	it("should able to enter school name sucessfully", function () {
         element(by.name('school_name')).sendKeys('Green Mount').then(function() {
           console.log("School name as Green mount entered sucessfully");
         });
    });
    it('Should able to enter feedback for school sucessfully:', function() {    
	  element(by.name('feedback')).sendKeys('Good School').then(function(text) {
	    console.log('Feedback entered sucessfully');
	  });
	});
	it('Should able to submmit the response sucessfully', function() {    
	  var submit=element(by.buttonText('Submit'));
	  browser.executeScript('window.scrollTo(0,1000);').then(function () {
      submit.click();
	  });
	  browser.sleep(3000);
	});
	it('Should able to open the submited response for Auto calculated values ', function() { 
       element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[5]/div/ul/li[./div/div/span='Auto Calculate Testing']")).element(by.css('.glyphicon-eye-open')).click();
	})
// 5881a131ad20c35801d087fa     20007
    it("Should able to check if value of boys' count is equal to boys' recount", function () {
	    expect(element(by.name('chk_eq')).getAttribute('value')).toEqual("true");
	    console.log('Equal element is validated successfully');
    });
    it("Should able to check if value of boys' count is greater than girls' count", function () {
	    expect(element(by.name('chk_gt')).getAttribute('value')).toEqual("true");
	    console.log('Greater than element is validated successfully');
    });
    it("Should able to check if value of girls' count is less to boys' count", function () {
	    expect(element(by.name('chk_lt')).getAttribute('value')).toEqual("true");
	    console.log('Less than element is validated successfully');
    });
    it("Should able to check if value of boys' count is not equal to boys' recount", function () {
	    expect(element(by.name('chk_neq')).getAttribute('value')).toEqual("false");
	    console.log('Not Equal element is validated successfully');
    });
    it("Should able to check if value of boys' count is greater than girls' count and value of boys' count is equal to boys' recount", function () {
	    expect(element(by.name('chk_and')).getAttribute('value')).toEqual("true");
	    console.log('"And" element is validated successfully');
    });
    it("Should able to check if value of boys' count is greater than girls' count or value of boys' count is not equal to boys' recount", function () {
	    expect(element(by.name('chk_or')).getAttribute('value')).toEqual("true");
	    console.log('"And" element is validated successfully');
    });
	it('Should able to Auto sum the value for boys and girls count:', function() {    
	  total_count=element(by.name('chk_sum'));
	  expect(total_count.getAttribute('value')).toEqual("900");
	    console.log('Expected sum 900 is validated successfully');
	});
	it('Should able to Auto subtract the value for boys and girls count:', function() {    
	  total_count=element(by.name('chk_sub'));
	  expect(total_count.getAttribute('value')).toEqual("100");
	    console.log('Expected sub 100 is validated successfully');
	});
	it('Should able to Auto multply the value for boys and girls count:', function() {    
	  total_count=element(by.name('chk_prod'));
	  expect(total_count.getAttribute('value')).toEqual("200000");
	    console.log('Expected sum 20000 is validated successfully');
	});	
	it('Should able to Auto divide the value for boys and girls count:', function() {    
	  total_count=element(by.name('chk_div'));
	  expect(total_count.getAttribute('value')).toEqual("1.25");
	    console.log('Expected quetiont 1.25 is validated successfully');
	});

    it("Should able to show max value between boys' count and girls' count", function () {
	    expect(element(by.name('chk_max')).getAttribute('value')).toEqual("500");
	    console.log('Max element is validated successfully');
    });
    it("Should able to show min value between boys' count and girls' count", function () {
	    expect(element(by.name('chk_min')).getAttribute('value')).toEqual("400");
	    console.log('Min element is validated successfully');
    });
    it("Should able to show sqrt of girls' count", function () {
	    expect(element(by.name('chk_sqrt')).getAttribute('value')).toEqual("20");
	    console.log('sqrt element is validated successfully');
    });
    it("Should able to show number of characters in school name", function () {
	    expect(element(by.name('chk_len')).getAttribute('value')).toEqual("11");
	    console.log('Len element is validated successfully');
    });
    it("Should able to check equality for text elements", function () {
	    expect(element(by.name('chk_txt_eq')).getAttribute('value')).toEqual("false");
	    console.log('Text Equal element is validated successfully');
    });
    it("Should able to trim extra spaces from school name", function () {
	    expect(element(by.name('chk_trim')).getAttribute('value')).toEqual("Green Mount");
	    console.log('Trim element is validated successfully');
    });
    it("Should able to count number of characters from left in school name", function () {
	    expect(element(by.name('chk_left')).getAttribute('value')).toEqual("Green");
	    console.log('Left element is validated successfully');
    });
    it("Should able to count number of characters from right in school name", function () {
	    expect(element(by.name('chk_right')).getAttribute('value')).toEqual("Mount");
	    console.log('right element is validated successfully');
    });
    it("Should able to change characters in school name to upper case", function () {
	    expect(element(by.name('chk_uc')).getAttribute('value')).toEqual("GREEN MOUNT");
	    console.log('Upper Case element is validated successfully');
    });
    it("Should able to change characters in school name to lower case", function () {
	    expect(element(by.name('chk_lc')).getAttribute('value')).toEqual("green mount");
	    console.log('Lower Case element is validated successfully');
    });
    it("Should able to change the characters in school name to proper format", function () {
	    expect(element(by.name('chk_proper')).getAttribute('value')).toEqual("Green Mount");
	    console.log('Proper element is validated successfully');
    });     
    it("Should able to show concated value for school name and tag ID", function () {
	    expect(element(by.name('chk_concat')).getAttribute('value')).toEqual("Green Mount20007");
	    console.log('Concat element is validated successfully');
    });   
    it("Should able to show tag id of selected state ", function () {
	    expect(element(by.name('tag_id')).getAttribute('value')).toEqual("20007");
	    console.log('Tag id element is validated successfully');
    });
    it("Should able to show boys' count successfully ", function () {
	    expect(element(by.name('num_val')).getAttribute('value')).toEqual("500");
	    console.log("Number 'keyword' element is validated successfully");
    });
    it("Should able to show school name successfully ", function () {
	    expect(element(by.name('text_val')).getAttribute('value')).toEqual("Green Mount");
	    console.log("Number 'keyword' element is validated successfully");
    });

});

describe('AutoCalculate delete:', function() {
    it("Should able to program page", function () {
	    element(by.xpath(locator.Programs)).click();
	});
    it("Should able to open 'Testing P3 Mobile App' program", function () {
	    var program_name=element.all(by.cssContainingText('.ng-binding','Testing P3 Mobile App'));
        browser.executeScript('window.scrollTo(0,1000);').then(function () {
           program_name.click();
        });
    });
    it("Should able to open 'AutoCalculate' profile", function () {
        element(by.css('a[href*="#/profiles"]')).click();
        var profile_name=element.all(by.cssContainingText('.ng-binding','AutoCalculate'));
        browser.executeScript('window.scrollTo(0,1000);').then(function () {
           profile_name.click();
        });
    });
    it("Should able to navigate to data entry page", function () {    
        element(by.cssContainingText(".btn.btn-default.btn-sm","View Data")).click();
    });
    it("Should able to delete 'Auto Calculate Testing' response if present", function () {    
        var response_name=element(by.xpath("//*[@id='content-main']/div[3]/p3-table/div[2]/table/tbody/tr[./td/a='Auto Calculate Testing']")).element(by.css('.glyphicon-remove'));
        browser.wait(EC.elementToBeClickable(response_name),10000);
        response_name.click();
        browser.switchTo().alert().accept();
        browser.sleep(3000);
        console.log("After testing auto calculate functionality, resultant response is deleted successfully")
    });
                
});    





