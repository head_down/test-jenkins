var dragNDrop = require('./drag.js');
describe('P3-App Login Testing', function() {
    it("should able to create a new program 'Precommit_Automation'", function() {
        browser.sleep(7000);
        var program=element(by.xpath("//td[text()='Programs']"));
        // browser.wait(EC.elementToBeClickable(program), 20000);
        program.click();

        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/p3-titlebar/div/div[2]/div[1]/div/button[1]")).click();
        browser.sleep(2000);
        element(by.xpath("//*[@id='newProgramDialog']/div/div/div[2]/div/input")).sendKeys("Precommit_Automation");
        element(by.buttonText('Save')).click();
        browser.sleep(3000);   
        console.log("A program created sucessfull with name 'Precommit_Automation'");
    });  
    it("should able to create a new profile 'Precommit_profile-1'", function() {  
        var ele=element(by.cssContainingText('.ng-binding.ng-scope', 'Precommit_Automation'));
        browser.executeScript('window.scrollTo(0,1000);').then(function () {
           ele.click();
         })
        browser.sleep(2000);
        element(by.css('a[href*="#/profiles"]')).click();
        browser.sleep(2000);       
        element(by.buttonText('Create First Profile')).click();
        browser.sleep(2000);
        element(by.name("name")).sendKeys("Precommit_profile-1");
        element(by.buttonText('Save')).click(); 
        browser.sleep(3000);
        console.log("A profile created sucessfull with name 'Precommit_profile-1'");
    });
    it("should able to create questionaries for 'Precommit_profile-1'", function() { 
        element(by.cssContainingText('.ng-binding.ng-scope', 'Precommit_profile-1')).click();
        browser.sleep(5000);
        var elementToDrag=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[1]/ul/li[1]"));
        var locationToDragTo=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li"));
        browser.executeScript(dragNDrop,elementToDrag.getWebElement(), locationToDragTo.getWebElement());
        
        // draging elements
        var source=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[1]/ul/li[2]"));
        var des=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[2]"));
        browser.executeScript(dragNDrop,source.getWebElement(), des.getWebElement());

        var source=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[1]/ul/li[3]"));
        var des=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[2]"));
        browser.executeScript(dragNDrop,source.getWebElement(), des.getWebElement());

        var source=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[1]/ul/li[4]"));
        var des=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[2]"));
        browser.executeScript(dragNDrop,source.getWebElement(), des.getWebElement());

        var source=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[1]/ul/li[5]"));
        var des=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[2]"));
        browser.executeScript(dragNDrop,source.getWebElement(), des.getWebElement());

        var source=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[1]/ul/li[6]"));
        var des=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[2]"));
        browser.executeScript(dragNDrop,source.getWebElement(), des.getWebElement());

        var source=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[1]/ul/li[7]"));
        var des=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[2]"));
        browser.executeScript(dragNDrop,source.getWebElement(), des.getWebElement());
        var source=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[1]/ul/li[8]"));
        var des=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[2]"));
        browser.executeScript(dragNDrop,source.getWebElement(), des.getWebElement());

        var source=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[1]/ul/li[9]"));
        var des=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[2]"));
        browser.executeScript(dragNDrop,source.getWebElement(), des.getWebElement());
        // interchnage name element position
        // var source=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[10]"));
        // var des=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[1]"));
        // browser.executeScript(dragNDrop,source.getWebElement(), des.getWebElement());
        // browser.sleep(5000);
    });  
    it("should able to create a tag",function(){
       browser.sleep(2000);
       element(by.xpath("//*[@id='nav']/li[7]/a/table/tbody/tr/td[2]")).click();
       element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/div/p[3]/button")).click();
       browser.sleep(2000);
       element(by.xpath("//*[@id='newTagDialog']/div/div/div[2]/div/input")).sendKeys("State");
       element(by.buttonText('Save')).click();
       browser.sleep(2000);
       element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/p3-list/ul/li/div/div[1]/a")).click();
       element(by.xpath("//*[@id='content-main']/p3-loaded/div/p3-hierarchy/div[2]/div/button")).click();
       element(by.xpath("//*[@id='nameField']")).sendKeys("Arunachal Pradesh");
       element(by.xpath("//*[@id='content-main']/p3-loaded/div/p3-hierarchy/div[2]/div/form/button[1]")).click();
       element(by.xpath("//*[@id='nameField']")).sendKeys("Assam");
       element(by.xpath("//*[@id='content-main']/p3-loaded/div/p3-hierarchy/div[2]/div/form/button[1]")).click();
       element(by.xpath("//*[@id='nameField']")).sendKeys("Bihar");
       element(by.xpath("//*[@id='content-main']/p3-loaded/div/p3-hierarchy/div[2]/div/form/button[1]")).click().then(function(){
        console.log("Tag created successfully");
       })
       element(by.xpath("//*[@id='nav']/li[6]/a/table/tbody/tr/td[2]")).click();
       element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/p3-list/ul/li/div/div[1]/a")).click();
    });



    it("should able to check Text element", function () {
        browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[1]/div/div[3]/a[1]/span")).click();
        var text_keyword=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[1]/input"));
        text_keyword.clear().then(function(){
            text_keyword.sendKeys("Test_Textbox");
        });
        var text_question=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[2]/input"));
        text_question.clear().then(function(){
            text_question.sendKeys("Testing Textbox element, enter some text.");
        });
        var text_exp=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[3]/input"));
        text_exp.clear().then(function(){
            text_exp.sendKeys("I am a Tester");
        })
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/p/input")).click().then(function(){
            console.log("Question for TextBox created successfully");
        }); 
        browser.sleep(2000);
    });
    it("should able to check select element", function () {
        browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[2]/div/div[3]/a[1]/span")).click();
        var select_keyword=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[1]/input"));
        select_keyword.clear().then(function(){
            select_keyword.sendKeys("Test_Select");
        });
        var select_question=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[2]/input"));
        select_question.clear().then(function(){
            select_question.sendKeys("Testing select element, select any element.");
        });
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[4]/select")).element(by.cssContainingText('option', 'State')).click();
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/p/input")).click().then(function(){
            console.log("Question for SelectBox created successfully");
        });
    });
    it("should able to check Profile reference element", function () {
        browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[3]/div/div[3]/a[1]/span")).click();
        var pr_keyword=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[1]/input"));
        pr_keyword.clear().then(function(){
            pr_keyword.sendKeys("Test_ProfileRef");
        })
        var pr_question=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[2]/input"));
        pr_question.clear().then(function(){
            pr_question.sendKeys("Testing profile_Ref element, enter some reference response.");
        })
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[5]/select")).element(by.cssContainingText('option', 'Precommit_profile-1')).click();
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/p/input")).click().then(function(){
            console.log("Question for Profile reference created successfully");
        });
    });
    it("should able to check photo element", function () {
        browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[4]/div/div[3]/a[1]/span")).click();
        var photo_keyword=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[1]/input"));
        photo_keyword.clear().then(function(){
            photo_keyword.sendKeys("Test_Photo");
        });
        var photo_question=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[2]/input"));
        photo_question.clear().then(function(){
            photo_question.sendKeys("Testing photo element, enter some photo.");
        });
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/p/input")).click().then(function(){
            console.log("Question for photo created successfully");
        });
    });
    it("should able to check paragraph element", function () {
        browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[5]/div/div[3]/a[1]/span")).click();
        var para_keyword=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[1]/input"));
        para_keyword.clear().then(function(){
            para_keyword.sendKeys("Test_Paragraph");
        });
        var para_question=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[2]/input"));
        para_question.clear().then(function(){
            para_question.sendKeys("Testing paragraph element, enter a paragraph.");
        });
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/p/input")).click().then(function(){
            console.log("Question for Paragraph created successfully");
        });
    });
    it("should able to check number element", function () {
        browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[6]/div/div[3]/a[1]/span")).click();
        var num_keyword=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[1]/input"));
        num_keyword.clear().then(function(){
            num_keyword.sendKeys("Test_Number");
        })
        var num_question=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[2]/input"));
        num_question.clear().then(function(){
            num_question.sendKeys("Testing number element, enter a Number.");
        })
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/p/input")).click().then(function(){
            console.log("Question for Number element created successfully");
        });

    });
    it("should able to check file element", function () {
        browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[7]/div/div[3]/a[1]/span")).click();
        var file_keyword=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[1]/input"));
        file_keyword.clear().then(function(){
            file_keyword.sendKeys("Test_File");
        });
        var file_question=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[2]/input"));
        file_keyword.click().then(function(){
            file_question.sendKeys("Testing file element, enter a file.");
        })
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/p/input")).click().then(function(){
            console.log("Question for file created successfully");
        }); 

    });
    it("should able to check date element", function () {
        browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[8]/div/div[3]/a[1]/span")).click();
        var date_keyword=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[1]/input"));
        date_keyword.clear().then(function(){
            date_keyword.sendKeys("Test_Date");
        });
        var date_question=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[2]/input"));
        date_question.clear().then(function(){
            date_question.sendKeys("Testing Date element, enter some date.");
        })
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/p/input")).click().then(function(){
            console.log("Question for date created successfully");
        });
        browser.sleep(2000);
    });
    it("should able to check toggle element", function () {
        browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[9]/div/div[3]/a[1]/span")).click();
        var toggle_keyword=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[1]/input"));
        toggle_keyword.clear().then(function(){
            toggle_keyword.sendKeys("Test_Toggle");
        });
        var toggle_question=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[2]/input"));
        toggle_question.clear().then(function(){
            toggle_question.sendKeys("Testing Toggle element, click on Toggle.");
        });
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/p/input")).click().then(function(){
            console.log("Question for Toggle created successfully");
        }); 

    });
    it("should able to check name element", function () {
        browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[1]/div/ul/li[10]/div/div[3]/a/span")).click();
        // element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[1]/input")).sendKeys("Test_File");
        var name_question=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/div[2]/input"));
        name_question.clear().then(function(){
        name_question.sendKeys("Testing name element, enter a name.");
        }) 
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[2]/div[2]/div[2]/div[2]/form/fieldset/p/input")).click().then(function(){
            console.log("Question for name element created successfully");
            console.log("PRECOMMIT profile creater Properly")
        });

    });

    // Creating Flows
    it("should able to create a Flows", function () {
        element(by.xpath("//*[@id='nav']/li[8]/a/table/tbody/tr/td[2]")).click();
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/div/p[3]/button")).click();
        browser.sleep(2000);
        element(by.xpath("//*[@id='newFlowDialog']/div/div/div[2]/div/input")).sendKeys("Precommit_flow_1");
        element(by.buttonText('Save')).click().then(function(){
        console.log("Flows created successfully")
        browser.sleep(2000);
        })
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/p3-list/ul/li/div/div[1]/a")).click();
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/div[1]/p/button")).click();
        var source=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/div[2]/div[2]/div[3]/div/ul/li"));
        var des=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/div[1]/div/ul"));
        browser.executeScript(dragNDrop,source.getWebElement(),des.getWebElement());
        browser.sleep(5000);
    });
    // creating project
    it("should able to create a project", function () {
        element(by.xpath("//*[@id='nav']/li[5]/a/table/tbody/tr/td[2]")).click();
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/div/p[3]/button")).click();
        browser.sleep(2000);
        element(by.xpath("//*[@id='newProjectDialog']/div/div/div[2]/div[1]/input")).sendKeys("Precommit_project");
        element(by.buttonText('Save')).click().then(function(){
        console.log("project created successfully");
        browser.sleep(3000);
        })
    });  
    it("should able to set project date", function () {  
        element(by.xpath("//*[@id='tree-root']/li/div/div/div[1]/span/a")).click();
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/div/button")).click();
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/div/ul/li/a")).click();
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/div/div[1]/div/ul/li/a")).click();
        element(by.xpath("//*[@id='settings']/form/fieldset/div[1]/div[1]/div/input")).sendKeys('09/19/2016');
        element(by.xpath("//*[@id='settings']/form/fieldset/div[1]/div[2]/div/input")).sendKeys('09/19/2017');
        element(by.buttonText('Save')).click().then(function(){
        console.log("Project date seted successfully")
        browser.sleep(4000);
        })

    });
    it("should able to assign project to user",function(){
        element(by.xpath("//*[@id='tabContainer']/ul/li[2]/a")).click();
        element(by.xpath("//*[@id='users']/p3-quick-add-user-with-approver/div/div/button")).click();
        element(by.xpath("//*[@id='userEmailField']")).sendKeys("dilipkumar10cse21@gmail.com");
        element(by.xpath("//*[@id='approverEmailField']")).sendKeys("dilip.rai@nextgenpms.com");
        element(by.xpath("//*[@id='users']/p3-quick-add-user-with-approver/div/div/form/table/tbody/tr/td[3]/button[1]")).click().then(function(){
        console.log("project assigned to dilipkumar10cse21@gmail.com successfully");
        browser.sleep(4000);
        });
    });
    it("should able to go back to task page ",function(){
        element(by.xpath("//*[@id='nav']/li[1]/a")).click();
    });
});


