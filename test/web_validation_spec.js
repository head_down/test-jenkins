var EC = protractor.ExpectedConditions;
describe('Category: Profiles   Sub-Category: Validation Test', function() {
    // browser.sleep(10000);
    it("should able to navigate to question page", function () {
        browser.sleep(7000);
        // var ele=element(by.xpath("//*[@id='pending']/div[10]/div[3]/div/div[1]/a"));
        var ele=element.all(by.css('a[href*="#/task/31459"]')).get(0);
        browser.executeScript('window.scrollTo(0,1000);').then(function () {
           ele.click();
         });
        // element(by.xpath("//*[@id='content-main']/p3-loaded/div/p3-titlebar/div/div[2]/div[1]/div/a")).click();
        element(by.xpath(".//*[@id='content-main']/p3-loaded/div/div[1]/p3-titlebar/div/div[2]/div[1]/div/a")).click();
        // element(by.buttonText('ADD ENTRY')).click();
    });
    // it('TC_13: Entering name for web validation Testing:', function() { 
    it('TC_13: Name element succesfully checked for validation:', function() {   
        element(by.name('name')).sendKeys("Web base validation Testing").then(function() {
        console.info("Validation name entered succesfully");
        });
    });
    // it('TC_14: Entering value which is not Alpha Numeric for textbox, Should generate warning msg:', function() { 
    it('TC_14: "Alphanumeric" rule Tested successfully ', function() {   
        var text_an=element(by.name('text_an'));
        text_an.sendKeys('!@43testing').then(function() {
        console.info("Non alpha numeric values entered in textbox, Checking 'alphanumeric'");
        });
    });
    // it('TC_15: Entering invalid email_id for textbox, Should generate warning msg:', function() {  
    it('TC_15: "Email" rule Tested successfully', function() {   
        var text_email=element(by.name('text_email'));
        text_email.sendKeys('dilipkumar10cse21@gmail').then(function() {
        console.info("Invalid email_id entered in textbox, Checking 'email'");
        });
    });
    // it('TC_16: Entering long value to exceeding max length for textbox, Should generate warning msg:', function() {
    it('TC_16: "Maxlen" rule Tested successfully', function() {   
        var text_maxlen=element(by.name('text_maxlen'));
        text_maxlen.sendKeys("Itanagar is a beutiful city where natural beauty is at its best.").then(function() {
        console.info("long sentence entered in textbox, Checking 'max length'");
        });
    });
    // it('Entering few characters which fails minimum number of character for textbox, Should generate warning msg:', function() {   
        it('TC_23: "Minlen" rule Tested successfully', function() {   
        var text_minlen=element(by.name('text_minlen'));
        text_minlen.sendKeys("I am less").then(function() {
        console.info("Few characters entered in textbox, Checking 'minlen'");
        });
    });
    it('Entering invalid mobile number for textbox, Should generate warning msg:', function() {   
        var text_mobile=element(by.name('text_mobile'));
        text_mobile.sendKeys("634").then(function() {
         // 987634
        console.info("Invalid mobile number entered in textbox, Checking 'mobile number'");
        });
    });
    it('Entering invalid url for textbox, Should generate warning msg:', function() {   
        var text_url=element(by.name('text_url'));
        text_url.sendKeys("app.p3fy.com").then(function() {
        console.info("Invalid url entered in textbox, Checking 'url'");
        });
    });
    it('Leaving data field empty for textbox, Should generate warning msg::', function() {   
        var text_req=element(by.name('text_req'));
        text_req.sendKeys("").then(function() {
        console.info("Data field blank, Checking 'required text'");
        });
    });
    it('Entering value which is not a whole number for number element, Should generate warning msg:', function() {   
        var number_wn=element(by.name('number_wn'));
        number_wn.sendKeys(68.57).then(function() {
        console.info("Fractional values entered in number element, Checking 'whole number'");
        });
    });
    it('Entering invalid mobile number for number element, Should generate warning msg:', function() {   
        var number_mobile=element(by.name('number_mobile'));
        // number_mobile.sendKeys("987634").then(function() {
        number_mobile.sendKeys("6348").then(function() {
        console.info("Invalid mobile number entered in number element, Checking 'mobile number'");
        });
    });
    it('Entering larger value for exceeding max limit in number element, Should generate warning msg:', function() {   
        var number_max=element(by.name('number_max'));
        number_max.sendKeys(600000).then(function() {
        console.info("larger value entered in number element, Checking 'maximum value'");
        });
    });
    it('Entering value Below a minimum limitin in number element, Should generate warning msg:', function() {   
        var number_min=element(by.name('number_min'));
        number_min.sendKeys(4000).then(function() {
        console.info("low value entered in number element, Checking 'minimum value'");
        });
    });
    it('Leaving number field empty, Should generate warning msg:', function() {   
        var number_req=element(by.name('number_req')).sendKeys("");
        number_req.then(function() {
        console.info("Number field blank, Checking 'required number'");
        });
    });
    it('Entering value which is not Alpha Numeric in paragraph element, Should generate warning msg:', function() {   
        var para_an=element(by.name('para_an'));
        para_an.sendKeys('!@43testing').then(function() {
        console.info("Non alpha numeric values entered in paragraph element, Checking 'alphanumeric'");
        });
    });
    it('Entering larger value for exceeding max limit in paragraph element, Should generate warning msg:', function() {   
        var para_maxlen=element(by.name('para_maxlen'));
        para_maxlen.sendKeys('The best preparation for tomorrow is doing your best today').then(function() {
        console.info("larger value entered in paragraph element, Checking 'maximum value'");
        });
    });
    it('Entering value Below a minimum limitin in paragraph element, Should generate warning msg:', function() {   
        var para_minlen=element(by.name('para_minlen'));
        para_minlen.sendKeys('para test').then(function() {
        console.info("low value entered in paragraph element, Checking 'minimum value'");
        });
    });
    it('Leaving paragraph field empty, Should generate warning msg:', function() {   
        var para_req=element(by.name('para_req')).sendKeys("");
        para_req.then(function() {
        console.info("paragraph field blank, Checking 'required number'");
        });
    });
    it('submitting the data', function() {
        element(by.buttonText('Submit')).click().then(function() {
        console.error('Submitting the project');
        });
    });
    // it('Should Click on Submit pop-up:', function() {
    //     element(by.buttonText('Submit Now')).click().then(function(text) {
    //     console.log('clicked on pop-up submit Now');
    //     });
    // });
    it('Should check the pop-up content and validate all required warning msg:', function() {
        browser.sleep(5000);
        expect(element.all(by.css('.p3-error')).get(0).getText()).toMatch(/Please enter valid alpha-numeric values../);
        expect(element.all(by.css('.p3-error')).get(1).getText()).toMatch(/please enter valid email-id../);
        expect(element.all(by.css('.p3-error')).get(2).getText()).toMatch(/please enter less than 20 characters../);
        expect(element.all(by.css('.p3-error')).get(3).getText()).toMatch(/please enter more than 10 characters../);
         expect(element.all(by.css('.p3-error')).get(4).getText()).toMatch(/Input required in mobile number format../);
        expect(element.all(by.css('.p3-error')).get(5).getText()).toMatch(/please enter valid url../);
        expect(element.all(by.css('.p3-error')).get(6).getText()).toMatch(/This input box can't be empty../);
        expect(element.all(by.css('.p3-error')).get(7).getText()).toMatch(/please enter a valid whole number../);
        expect(element.all(by.css('.p3-error')).get(8).getText()).toMatch(/please enter a valid mobile number../);
        expect(element.all(by.css('.p3-error')).get(9).getText()).toMatch(/please input a value less than 500000../);
        expect(element.all(by.css('.p3-error')).get(10).getText()).toMatch(/please Input a number greater than 5000../);
        expect(element.all(by.css('.p3-error')).get(11).getText()).toMatch(/Required number field can't be empty../);
        expect(element.all(by.css('.p3-error')).get(12).getText()).toMatch(/Please enter valid alpha-numeric values for paragraph../);
        expect(element.all(by.css('.p3-error')).get(13).getText()).toMatch(/Exceeded maximum limit of 40 characters for paragraph../);
        expect(element.all(by.css('.p3-error')).get(14).getText()).toMatch(/please enter more than 10 characters../);
        expect(element.all(by.css('.p3-error')).get(15).getText()).toMatch(/Required field can't be empty for paragraph../);
        console.log("All validation scenario tested properly");
        browser.sleep(2000);
    });
    it('Should re-enter the correct values and submit the response', function() {
        var text_an=element(by.name('text_an'));
        text_an.clear().then(function() {
          text_an.sendKeys('web_app_testing');
        })
        var text_email=element(by.name('text_email'));
        text_email.clear().then(function() {
          text_email.sendKeys('dilipkumar10cse21@gmail.com');
        })
        var text_maxlen=element(by.name('text_maxlen'));
        text_maxlen.clear().then(function() {
          text_maxlen.sendKeys('Come to Bangalore');
        })
        var text_minlen=element(by.name('text_minlen'));
        text_minlen.clear().then(function() {
          text_minlen.sendKeys('Its cool place');
        })
        var text_mobile=element(by.name('text_mobile'));
        text_mobile.clear().then(function() {
          text_mobile.sendKeys(9035876465);
        })
        var text_url=element(by.name('text_url'));
        text_url.clear().then(function() {
          text_url.sendKeys('https://app.p3fy.com/');
        })
        var text_req=element(by.name('text_req'));
        text_req.clear().then(function() {
          text_req.sendKeys('Bangalore is awesome place');
        })
        var number_wn=element(by.name('number_wn'));
        number_wn.clear().then(function() {
          number_wn.sendKeys(50000);
        })
        var number_mobile=element(by.name('number_mobile'));
        number_mobile.clear().then(function() {
          number_mobile.sendKeys(9538876465);
        })
        var number_max=element(by.name('number_max'));
        number_max.clear().then(function() {
          number_max.sendKeys(500000);
        })
        var number_min=element(by.name('number_min'));
        number_min.clear().then(function() {
          number_min.sendKeys(30000);
        })
        var number_req=element(by.name('number_req'));
        number_req.clear().then(function() {
          number_req.sendKeys(5000);
        })
        var para_an=element(by.name('para_an'));
        para_an.clear().then(function() {
          para_an.sendKeys('Paragraph_testing');
        })
        var para_maxlen=element(by.name('para_maxlen'));
        para_maxlen.clear().then(function() {
          para_maxlen.sendKeys('Do not wait for tomorrow');
        })
        var para_minlen=element(by.name('para_minlen'));
        para_minlen.clear().then(function() {
          para_minlen.sendKeys('today is reality, tomorrow is virtuality');
        });
        var para_req=element(by.name('para_req'));
        para_req.clear().then(function() {
          para_req.sendKeys('What we think, we become.');
          console.log("correct values entered successfully");
        });
        para_req.click();
        // browser.sleep(5000);
    });
    // it('submitting the data after entering correct values', function() {
    //     browser.sleep(5000);
    //     var ele=element(by.buttonText('Submit'));
    //     // browser.executeScript('window.scrollTo(0,1000);').then(function () {
    //     //    ele.click();
    //     browser.actions().mouseMove(ele).click().perform();
    //        console.info('clicking on submit button and response submitted successfuly');
    //     // });
    //     browser.sleep(5000);
    // });
    // it('submitting the data', function() {
    //     browser.sleep(5000);
    //     var ele=element(by.buttonText('Submit'));
    //     browser.executeScript('window.scrollTo(0,1000);').then(function () {
    //       ele.click();
    //     console.error('Submitting the project');
    //     browser.sleep(5000);
    //     });
    // });
    // it('should click on ok pop-up if its come up ', function() { 
    //       var OkLink =element(by.buttonText('Submit'));
    //       // var ele= element(by.xpath("//*[@id='content-main']/p3-loaded/div/form/fieldset/div[18]/p/button"));
    //       // browser.wait(EC.elementToBeClickable(OkLink), 20000);
    //       // OkLink.click();
    //       // console.log("ok button clicked properly after syncing, So School response sumbitted.");
    //     //    browser.executeScript('window.scrollTo(0,1000);').then(function () {
    //     //    OkLink.click();
    //     // }); 
    //     browser.actions().mouseMove(OkLink).click().perform();      
    //     browser.sleep(5000);
    // });
    // it('Leaving paragraph field empty, Should generate warning msg:', function() {   
    //     browser.sleep(3000);
    //     var para_req=element(by.name('toggle_val'));
    //     browser.actions().mouseMove(para_req).click().perform().then(function() {
    //     console.error('Toggle clicked properly');
    //     });
    // });
    it('This additional textbox is added for successful submition of data:', function() {   
        var para_req=element(by.name('text_val')).sendKeys("my testing");
        para_req.then(function() {
        console.info("paragraph field blank, Checking 'required number'");
        });
        browser.sleep(3000);
    });
    it('submitting the data', function() {
        // element(by.buttonText('Submit')).click().then(function() {
        // console.error('Submitting the project');
        // });
        browser.sleep(3000);
        var OkLink =element(by.buttonText('Submit'));
        // browser.executeScript('window.scrollTo(0,2000);').then(function () {
        browser.actions().mouseMove(OkLink).click().perform(); 
        // });
        browser.sleep(5000);
    });

});

