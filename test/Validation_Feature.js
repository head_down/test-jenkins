var home_page = function() {

    var FunctionLibrary = require('./FunctionLibrary.js')

    var waitTime=10000;
    var initialScrollValue=0;
    var finalScrollValue=1000;
    var ProfileLocator = element.all(by.css('a[href*="#/task/31459"]')).get(0);
    var AddEntryLocator = element(by.xpath("//*[contains(text(),'ADD ENTRY')]"));
    var ValidationTestNameLocator = element(by.name('name'));
    var anTextboxLocator = element(by.name('text_an'));
    var SubmitLocator = element(by.buttonText('Submit'));
    var alpahNumericErrorMsgLocator = element(by.xpath("//*[contains(text(),'Please enter valid alpha-numeric values..')]"));
    var emailTextboxLocator = element(by.name('text_email'));
    var emailErrorMsgLocator = element(by.xpath("//*[contains(text(),'please enter valid email-id')]"));
    var maxLenTextboxLocator = element(by.name('text_maxlen'));
    var maxLenErrorMsgLocator = element(by.xpath("//*[contains(text(),'please enter less than 20 characters')]"));
    var minLenTextboxLocator = element(by.name('text_minlen'));
    var minLenErrorMsgLocator = element(by.xpath("//*[contains(text(),'please enter more than 10 characters')]"));
    var mobileNumTextboxLocator = element(by.name('text_mobile'));
    var mobileNumErrorMsgLocator = element(by.xpath("//*[contains(text(),'Input required in mobile number format')]"));
    var urlTextboxLocator = element(by.name('text_url'));
    var urlErrorMsgLocator = element(by.xpath("//*[contains(text(),'please enter valid url')]"));
    var requiredTextboxLocator = element(by.name('text_req'));
    var requiredTextErrorMsgLocator = element(by.xpath("//*[contains(text(),'Required inputbox can not be empty')]"));
    var wholeNumberLocator = element(by.name('number_wn'));
    var wholeNumberErrorMsgLocator = element(by.xpath("//*[contains(text(),'please enter a valid whole number')]"));
    var mobileNumberLocator = element(by.name('number_mobile'));
    var mobileNumberErrorMsgLocator = element(by.xpath("//*[contains(text(),'please enter a valid mobile number')]"));
    var maxNumberLocator = element(by.name('number_max'));
    var maxNumberErrorMsgLocator = element(by.xpath("//*[contains(text(),'please input a value less than 500000')]"));
    var minNumberLocator = element(by.name('number_min'));
    var minNumberErrorMsgLocator = element(by.xpath("//*[contains(text(),'please Input a number greater than 5000')]"));
    var requiredNumberLocator = element(by.name('number_req'));
    var requiredNumberErrorMsgLocator = element(by.xpath("//*[contains(text(),'Required number field can not be empty')]"));
    var anParagraphLocator = element(by.name('para_an'));
    var anParagraphErrorMsgLocator = element(by.xpath("//*[contains(text(),'Please enter valid alpha-numeric values for paragraph')]"));
    var maxlenParagraphLocator = element(by.name('para_maxlen'));
    var maxlenParagraphErrorMsgLocator = element(by.xpath("//*[contains(text(),'Exceeded maximum limit of 40 characters for paragraph')]"));
    var minlenParagraphLocator = element(by.name('para_minlen'));
    var minlenParagraphErrorMsgLocator = element(by.xpath("//*[contains(text(),'please enter more than 20 characters')]"));
    var requiredParagraphLocator = element(by.name('para_req'));
    var requiredParagraphErrorMsgLocator = element(by.xpath("//*[contains(text(),'Required paragraph field can not be empty')]"));

    // Sending input value to _____ field.
    // Getting text from ______ field.

    var ProfileName = 'Validation Profile';
    var AddEntryName = 'Add entry button';
    var ValidationName = 'name inputbox';
    var anTextboxName = 'Alpha Numeric inputbox';
    var SubmitName = 'submit button';
    var alpahNumericErrorMsgName = 'alpahNumeric ErrorMsg';
    var emailTextboxName = 'email inputbox';
    var emailErrorMsgName = 'emailID ErrorMsg';
    var maxLenTextboxName = 'max length text';
    var maxLenErrorMsgName = 'maxlen ErrorMsg';
    var minLenTextboxName = 'min length text';
    var minLenErrorMsgName = 'minlen ErrorMsg';
    var mobileNumTextboxName = 'mobile_num. text';
    var mobileNumErrorMsgName = 'mobile ErrorMsg';
    var urlTextboxName = 'url text';
    var urlErrorMsgName = 'url ErrorMsg';
    var requiredTextboxName = 'required text';
    var requiredTextErrorMsgName = 'required text ErrorMsg';
    var wholeNumberName = 'whole_num. number';
    var wholeNumberErrorMsgName = 'whole_num ErrorMsg';
    var mobileNumberName = 'mobile_num. number';
    var mobileNumberErrorMsgName = 'mobile_num. ErrorMsg';
    var maxNumberName = 'max_num number';
    var maxNumberErrorMsgName = 'max_num ErrorMsg';
    var minNumberName = 'min_num number';
    var minNumberErrorMsgName = 'min_num ErrorMsg';
    var requiredNumberName = 'required_num number';
    var requiredNumberErrorMsgName = 'required_num ErrorMsg';
    var anParagraphName = 'alpahNumeric paragraph';
    var anParagraphErrorMsgName = 'alpahNumeric paragraph ErrorMsg';
    var maxlenParagraphName = 'max length paragraph';
    var maxlenParagraphErrorMsgName = 'maxlen paragraph ErrorMsg';
    var minlenParagraphName = 'min length paragraph';
    var minlenParagraphErrorMsgName = 'minlen paragraph ErrorMsg';
    var requiredParagraphName = 'required paragraph';
    var requiredParagraphErrorMsgName = 'required paragraph ErrorMsg';



    this.navigateToLoginPageProd = function() {

        //browser.ignoreSynchronization = false;

        browser.get('https://app.p3fy.com/');

        browser.manage().window().maximize(); // To maximize the browser to full screen size.

        browser.waitForAngular(); //To wait for page to be fully loaded.
    };


    this.clickOnProfileName = function() {

        FunctionLibrary.scrollClick(ProfileLocator, ProfileName);
    };
    this.clickOnAddEntry = function() {

        FunctionLibrary.click(AddEntryLocator, AddEntryName);

    };
    this.enterValidationTestName = function(value) {

        FunctionLibrary.sendKeys(ValidationTestNameLocator, value, ValidationName);
    };
    this.getEnteredValidationTestName = function() {

        return FunctionLibrary.getAttributeValue(ValidationTestNameLocator, 'value', ValidationName);

    };
    this.enterNonAlphaNumericValue = function(value) {

        FunctionLibrary.sendKeys(anTextboxLocator, value, anTextboxName);
    };
    this.clickOnSubmit = function() {

        FunctionLibrary.click(SubmitLocator, SubmitName);
    };
    this.getalpahNumericErrorMsg = function() {

        return FunctionLibrary.waitThengetText(alpahNumericErrorMsgLocator, alpahNumericErrorMsgName,waitTime);

    };

    this.enterWrongEmailID = function(value) {

        FunctionLibrary.sendKeys(emailTextboxLocator, value, emailTextboxName);
    };
    this.getWrongEmailIDErrorMsg = function() {

        return FunctionLibrary.waitThengetText(emailErrorMsgLocator,emailErrorMsgName,waitTime);

    };
    this.enterMaxlenCheckValue = function(value) {

        FunctionLibrary.sendKeys(maxLenTextboxLocator, value, maxLenTextboxName);
    };
    this.getMaxlenCheckErrorMsg = function() { 

        return FunctionLibrary.waitThengetText(maxLenErrorMsgLocator,maxLenErrorMsgName,waitTime);

    };
    this.enterMinlenCheckValue = function(value) {

        FunctionLibrary.sendKeys(minLenTextboxLocator, value, minLenTextboxName);
    };
    this.getMinlenCheckErrorMsg = function() {

        return FunctionLibrary.waitThengetText(minLenErrorMsgLocator,minLenErrorMsgName,waitTime);

    };
    this.enterMobileNumberToTextbox = function(value) {

        FunctionLibrary.sendKeys(mobileNumTextboxLocator, value, mobileNumTextboxName);
    };
    this.getMobileErrorMsg = function() {

        return FunctionLibrary.waitThengetText(mobileNumErrorMsgLocator,mobileNumErrorMsgName,waitTime);

    };
    this.enterUrlValue = function(value) {

        FunctionLibrary.sendKeys(urlTextboxLocator, value, urlTextboxName);
    };
    this.getUrlErrorMsg = function() {

        return FunctionLibrary.waitThengetText(urlErrorMsgLocator,urlErrorMsgName,waitTime);

    };
    this.enterValueToRequiredTextbox= function(value) {

        FunctionLibrary.sendKeys(requiredTextboxLocator, value, requiredTextboxName);
    };
    this.getRequiredTextboxErrorMsg = function() {

        return FunctionLibrary.waitThengetText(requiredTextErrorMsgLocator,requiredTextErrorMsgName,waitTime);

    };
    this.enterWholeNumber= function(value) {

        FunctionLibrary.sendKeys(wholeNumberLocator, value, wholeNumberName);
    };
    this.getWholeNumberErrorMsg = function() {

        return FunctionLibrary.waitThengetText(wholeNumberErrorMsgLocator,wholeNumberErrorMsgName,waitTime);

    };
    this.enterMobileNumber= function(value) {

        FunctionLibrary.sendKeys(mobileNumberLocator, value, mobileNumberName);
    };
    this.getMobileNumberErrorMsg = function() {

        return FunctionLibrary.waitThengetText(mobileNumberErrorMsgLocator,mobileNumberErrorMsgName,waitTime);

    };
    this.enterMaxNumber= function(value) {

        FunctionLibrary.sendKeys(maxNumberLocator, value, maxNumberName);
    };
    this.getMaxNumberErrorMsg = function() {

        return FunctionLibrary.waitThengetText(maxNumberErrorMsgLocator,maxNumberErrorMsgName,waitTime);

    };
    this.enterMinNumber= function(value) {

        FunctionLibrary.sendKeys(minNumberLocator, value, minNumberName);
    };
    this.getMinNumberErrorMsg = function() {

        return FunctionLibrary.waitThengetText(minNumberErrorMsgLocator,minNumberErrorMsgName,waitTime);

    };
    this.enterRequiredNumber= function(value) {

        FunctionLibrary.sendKeys(requiredNumberLocator, value, requiredNumberName);
    };
    this.getRequiredNumberErrorMsg = function() {

        return FunctionLibrary.waitThengetText(requiredNumberErrorMsgLocator,requiredNumberErrorMsgName,waitTime);

    };
    this.enterNonAlphaNumericValuePgh= function(value) {

        FunctionLibrary.sendKeys(anParagraphLocator, value, anParagraphName);
    };
    this.getAlphaNumericValuePghErrorMsg = function() {

        return FunctionLibrary.waitThengetText(anParagraphErrorMsgLocator,anParagraphErrorMsgName,waitTime);

    };
    this.enterMaxCheckValuePgh= function(value) {

        FunctionLibrary.sendKeys(maxlenParagraphLocator, value, maxlenParagraphName);
    };
    this.getMaxCheckValuePghErrorMsg = function() {

        return FunctionLibrary.waitThengetText(maxlenParagraphErrorMsgLocator,maxlenParagraphErrorMsgName,waitTime);

    };
    this.enterMinCheckValuePgh= function(value) {

        FunctionLibrary.sendKeys(minlenParagraphLocator, value, minlenParagraphName);
    };
    this.getMinCheckValuePghErrorMsg = function() {

        return FunctionLibrary.waitThengetText(minlenParagraphErrorMsgLocator,minlenParagraphErrorMsgName,waitTime);

    };
    this.enterRequiredValuePgh= function(value) {

        FunctionLibrary.sendKeys(requiredParagraphLocator, value, requiredParagraphName);
    };
    this.getRequiredValuePghErrorMsg = function() {

        return FunctionLibrary.waitThengetText(requiredParagraphErrorMsgLocator,requiredParagraphErrorMsgName,waitTime);

    };

};
module.exports = new home_page();
