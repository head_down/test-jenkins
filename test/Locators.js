exports.config = {
	MyTasks:"//*[@id='nav']/li[1]/a",
	Programs:"//td[text()='Programs']",  //self created locator with html element name
    Profiles:"//*[@id='content-main']/p3-loaded/div/div/div[2]/div[1]/a/p[2]",
	AddEntry:"//*[@id='content-main']/p3-loaded/div/div/p3-titlebar/div/div[2]/div[1]/div/button[1]/span",
	TaskAddEntry:"//*[@id='content-main']/p3-loaded/div/div[1]/p3-titlebar/div/div[2]/div[1]/div/a",
	Projects:"//*[@id='content-main']/p3-loaded/div/div/div[1]/div[2]/a/p[2]",
    Flows: "//*[@id='nav']/li[8]/a/table/tbody/tr/td[2]",
	

    
 // Programs page locators:
    closeButton:"//*[@id='newProgramDialog']/div/div/div[3]/button",
    plusIcon:"//*[@id='content-main']/p3-loaded/div/div/p3-list/div[2]/div/button",
    tickMark:"//*[@id='content-main']/p3-loaded/div/div/p3-list/div[2]/div/form/button[1]",
    crossMark:"//*[@id='content-main']/p3-loaded/div/div/p3-list/div[2]/div/form/button[2]",
    imageUnload:"//*[@id='profile']/form/fieldset/div/div[2]/div/div[1]/div[2]/a/span",  
    addUserPgm: "//*[@id='users']/p/p3-quick-add-user/div/div/button",
    userRole: "//*[@id='users']/p/p3-quick-add-user/div/div/form/table/tbody/tr/td[2]/select",
    addUserTick: "//*[@id='users']/p/p3-quick-add-user/div/div/form/table/tbody/tr/td[3]/button[1]",
    addUserCrossMark: "//*[@id='users']/p/p3-quick-add-user/div/div/form/table/tbody/tr/td[3]/button[2]",
    recordsCountPgm:"//*[@id='users']/p/p3-table/div[2]/table/tbody/tr[1]/th[4]/span/small",
    userTabPgm:"//*[@id='tabContainer']/ul/li[4]/a",


// Projects page locators:
    CreateProject:"//*[@id='content-main']/p3-loaded/div/div/div/p[3]/button",   

// Flows page locators;
    plusFlow:"//*[@id='content-main']/p3-loaded/div/div/p3-list/div[2]/div/button",
    flowName:"//*[@id='content-main']/p3-loaded/div/a/h4", 
    flowNameInput:"/*[@id='content-main']/p3-loaded/div/form/div/input",
    flowTick:"//*[@id='content-main']/p3-loaded/div/form/div/span/button[1]",
    flowCross:"//*[@id='content-main']/p3-loaded/div/form/div/span/button[2]",
    phaseNameInput:"//*[@id='content-main']/p3-loaded/div/div/div[1]/div/div/div[1]/form/div/input",
    phaseTick:"//*[@id='content-main']/p3-loaded/div/div/div[1]/div/div/div[1]/form/div/span/button[1]",
    phaseCross:"//*[@id='content-main']/p3-loaded/div/div/div[1]/div/div/div[1]/form/div/span/button[2]",
    phaseDelete:"//*[@id='content-main']/p3-loaded/div/div/div[1]/div/div/div[2]/a/span",


}
