
exports.config = {

  // Spec patterns are relative to the location of this config.
  
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['spec_mem.js'],

  capabilities: {
    'browserName': 'chrome',
    'chromeOptions': {'args': ['lang=en-GB',  'enable-precise-memory-info' , 'js-flags=--expose-gc', 'no-sandbox']}
    // args: ['enable-memory-info'],
  },
   // baseUrl: 'https://app.p3fy.com/#/login',
    baseUrl: 'http://localhost:9000/',

  jasmineNodeOpts: {
    onComplete: null,
    isVerbose: false,
    showColors: true,
    includeStackTrace: true,
    defaultTimeoutInterval: 10000
  }
};

if (process.env.TRAVIS) {
  exports.config.capabilities.chromeOptions.binary = __dirname + '/../chrome-linux/chrome';
}
