var Login_Page = require('./Login_Page.js');

var Validation_Feature = require('./Validation_Feature.js');

var loginData = require('./Login_Page_Data.json');

var testData = require('./Validation_Data.json');

var EC = protractor.ExpectedConditions;

describe('Category: Profiles, Sub-Category: Validation Test', function() {

    beforeAll(function(done) {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 120000;

        done();
    });

    beforeEach(function() {

        Validation_Feature.navigateToLoginPageProd();

        Login_Page.enterEmailAddress(loginData[0].emailID);

        Login_Page.enterPassword(loginData[0].password);

        Login_Page.clickOnSignIn();

        Validation_Feature.clickOnProfileName();

        Validation_Feature.clickOnAddEntry();

    });

    it('TC_13: Should enter name for validation test succesfully', function() {

        Validation_Feature.enterValidationTestName(testData[0].name);

        expect(Validation_Feature.getEnteredValidationTestName()).toEqual(testData[0].name);
    });

    it('TC_14: Should accept only alphanumeric values in textbox', function() {

        Validation_Feature.enterNonAlphaNumericValue(testData[1].nonAlphaNumericValue);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getalpahNumericErrorMsg()).toMatch(testData[2].errorMsgForAlphaNumeric);
    });
    it('TC_15: Should accept only emailID in textbox', function() {

        Validation_Feature.enterWrongEmailID(testData[1].WrongEmailID);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getWrongEmailIDErrorMsg()).toMatch(testData[2].errorMsgForEmailID);
    });
    it('TC_15: Should check maxlen input limit of textbox', function() {

        Validation_Feature.enterMaxlenCheckValue(testData[1].wrongMaxLenInput);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getMaxlenCheckErrorMsg()).toMatch(testData[2].errorMsgForMaxlen);
    });
    it('TC_15: Should check minlen input limit of textbox', function() {

        Validation_Feature.enterMinlenCheckValue(testData[1].wrongMinLenInput);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getMinlenCheckErrorMsg()).toMatch(testData[2].errorMsgForMinlen);
    });
    it('TC_15: Should accept only mobile number to textbox', function() {

        Validation_Feature.enterMobileNumberToTextbox(testData[1].wrongMobileInput);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getMobileErrorMsg()).toMatch(testData[2].errorMsgForMobile);
    });
    it('TC_15: Should accept only URL value into textbox', function() {

        Validation_Feature.enterUrlValue(testData[1].wrongUrlInput);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getUrlErrorMsg()).toMatch(testData[2].errorMsgForUrl);
    });
    it('TC_15: Should not accept empty textbox', function() {

        Validation_Feature.enterValidationTestName(testData[0].name);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getRequiredTextboxErrorMsg()).toMatch(testData[2].errorMsgForRequiredText);
    });
    it('TC_15: Should accept only whole number to number field', function() {

        Validation_Feature.enterWholeNumber(testData[1].wrongWholeNumInput);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getWholeNumberErrorMsg()).toMatch(testData[2].errorMsgForWholeNum);
    });
    it('TC_15: Should accept only mobile number to number field', function() {

        Validation_Feature.enterMobileNumber(testData[1].wrongMobileNumInput);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getMobileNumberErrorMsg()).toMatch(testData[2].errorMsgForMobileNum);
    });
    it('TC_15: Should not accept value max than 500000 to number field', function() {

        Validation_Feature.enterMaxNumber(testData[1].wrongMaxInput);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getMaxNumberErrorMsg()).toMatch(testData[2].errorMsgForMaxValue);
    });
    it('TC_15: Should not accept value min than 4000 to number field', function() {

        Validation_Feature.enterMinNumber(testData[1].wrongMinInput);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getMinNumberErrorMsg()).toMatch(testData[2].errorMsgForMinValue);
    });
    it('TC_15: Should not accept empty number field', function() {

        Validation_Feature.enterValidationTestName(testData[0].name);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getRequiredNumberErrorMsg()).toMatch(testData[2].errorMsgForRequiredNum);
    });
    it('TC_15: Should accept only alphanumeric values into paragraph', function() {

        Validation_Feature.enterNonAlphaNumericValuePgh(testData[1].wrongANPghInput);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getAlphaNumericValuePghErrorMsg()).toMatch(testData[2].errorMsgForANPgh);
    });

    it('TC_15: Should check minlen input limit of paragraph', function() {

        Validation_Feature.enterMinCheckValuePgh(testData[1].wrongMinLenPghInput);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getMinCheckValuePghErrorMsg()).toMatch(testData[2].errorMsgForMinlenPgh);
    });
    it('TC_15: Should check maxlen input limit of paragraph', function() {

        Validation_Feature.enterMaxCheckValuePgh(testData[1].wrongMaxLenPghInput);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getMaxCheckValuePghErrorMsg()).toMatch(testData[2].errorMsgForMaxlenPgh);
    });
    it('TC_15: Should not accept empty paragraph', function() {

        Validation_Feature.enterValidationTestName(testData[0].name);

        Validation_Feature.clickOnSubmit();

        expect(Validation_Feature.getRequiredValuePghErrorMsg()).toMatch(testData[2].errorMsgForRequiredPgh);
    });


    afterEach(function() {

        //clear session
        browser.executeScript('window.sessionStorage.clear();');

        //clear local storage
        browser.executeScript('window.localStorage.clear();');

    });

});
