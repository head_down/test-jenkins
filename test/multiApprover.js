describe('Testing multi approver functionality for web application', function() {
	it("should able to navigate to response page", function () {
        browser.sleep(7000);
        var ele=element.all(by.cssContainingText('.ng-binding','Create multiApprover - Basic Info')).get(1);
        browser.executeScript('window.scrollTo(0,1000);').then(function () {
           ele.click();
         })
        browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[1]/p3-titlebar/div/div[2]/div[1]/div/a")).click();

        browser.sleep(3000);
    });
    it("should able to input name for multiApprover Testing", function () {
        element(by.name('name')).sendKeys('multiApprover Testing').then(function() {
          console.log('Name entered sucessfully');
        });
    });
    it("should able to input game name ", function () {
        element(by.name('game_name')).sendKeys("quiz").then(function() {
          console.log('Game type entered sucessfully');
        });
    });
    it("should able to input flag ratio", function () {
        element(by.name('national_flag')).sendKeys("2:3").then(function() {
          console.log('flag ratio entered sucessfully');
        });
    });
    it("should able to input anthem stanza", function () {
        element(by.name('national_anthem')).sendKeys('only the first stanza').then(function() {
          console.log('Anthem stanza tested sucessfully');
        });
    });
    it("should able to input value for satyameva_jayate element", function () {
        element(by.name('satyameva_jayate')).sendKeys('Mundak').then(function() {
          console.log('satyameva_jayate element tested sucessfully');
        });
    });
    it("should able to submmit the project", function () {
        element(by.buttonText('Submit')).click().then(function() {
          console.log('Response submmited sucessfully');
        });
        browser.sleep(3000);
    });
    it("Should able to logout successfully", function () {
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/ul/li[9]/a")).click().then(function(){
          console.log('Logout successfully after submitting the response');
        });
        browser.sleep(2000);
    });

  // checking the status of response after submitting the response
    it("Should able to login to first approver account", function () {
    	element(by.model('credentials.email')).sendKeys('dilip.rai@nextgenpms.com');
        element(by.model('credentials.password')).sendKeys(121);
        element(by.buttonText('Sign In')).click().then(function() {
          console.log('Login successfully for first approver');
          browser.sleep(5000);
        });
    });
    it("Should able to check status of response first approver", function () {
    	browser.sleep(3000);
        var ele=element.all(by.cssContainingText('.ng-binding.ng-scope','Approve multiApprover - Basic Info')).get(1);
        // var ele=element.all(by.css('a[href*="#/task/32011"]')).get(1);
        // browser.executeScript('window.scrollTo(0,1000);').then(function () {
        	browser.executeScript("arguments[0].scrollIntoView();", ele.getWebElement());
            ele.click();
        // });
       var loc=element(by.css('.ng-scope.p3-PENDING')).element(by.cssContainingText('.ng-binding.ng-scope','multiApprover Testing'));
         loc.isPresent().then(function(present){
         	if(present){
         		console.log("Element is in active state(blue), which is expected for first approver");
         	}else{
         		console.log("Error:Element doesn't in active state, which is not expected for first approver");
         	}
         });
    });
    it("Should able to logout from first approver account successfully", function () {
    	browser.sleep(5000);
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/ul/li[9]/a")).click().then(function(){
          console.log('Logout successfully from first approver account');
        });
        browser.sleep(2000);
    });
    it("Should able to login to second approver account", function () {
    	element(by.model('credentials.email')).sendKeys('srirang.ranjalkar@nextgenpms.com');
        element(by.model('credentials.password')).sendKeys(1234);
        element(by.buttonText('Sign In')).click().then(function(){
          console.log('logged in for second approver account');
        });
        browser.sleep(5000);
    });
    it("Should able to check status of response second approver", function () {
        var ele=element.all(by.cssContainingText('.ng-binding','Approve multiApprover - Basic Info')).get(1);
        	browser.executeScript("arguments[0].scrollIntoView();", ele.getWebElement());
            ele.click();
        var loc=element(by.css('.ng-scope.p3-PENDING')).element(by.cssContainingText('.ng-binding.ng-scope','multiApprover Testing'));
         loc.isPresent().then(function(present){
         	if(present){
            console.log("Error:Element is in active state, which is not expected for second approver");
         	}else{
         		console.log("Element is not in active state(grey), which is expected for second approver");
         	}
         });
    });
    it("Should able to logout from second approver account successfully", function () {
    	browser.sleep(5000);
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/ul/li[9]/a")).click().then(function(){
          console.log('Logout successfully from second approver account');
        });
        browser.sleep(2000);
    });
    it("Should able to login to third approver account", function () {
    	element(by.model('credentials.email')).sendKeys('sai.kaushik@nextgenpms.com');
        element(by.model('credentials.password')).sendKeys("kaushik123");
        element(by.buttonText('Sign In')).click().then(function(){
          console.log('logged in for second approver account');
        });
        browser.sleep(5000);
    });
    it("Should able to check status of response for third approver", function () {
    	element(by.buttonText("Expand All")).click();
        var ele=element.all(by.cssContainingText('.ng-binding','Approve multiApprover - Basic Info')).get(1);
        	browser.executeScript("arguments[0].scrollIntoView();", ele.getWebElement());
            ele.click();
        var loc=element(by.css('.ng-scope.p3-PENDING')).element(by.cssContainingText('.ng-binding.ng-scope','multiApprover Testing'));
         loc.isPresent().then(function(present){
         	if(present){
         		console.log("Error:Element is in active state, which is not expected for third approver");
         	}else{
            console.log("Element is not in active state(grey), which is expected for third approver");
         	}
         });
    });
    it("Should able to logout from third approver account successfully", function () {
    	browser.sleep(5000);
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/ul/li[9]/a")).click().then(function(){
          console.log('Logout successfully from third approver account');
        });
        browser.sleep(2000);
    });

  // Now rejecting the response by first approver and checking the status for other approver
    it("Should able to login to first approver account to reject the response ", function () {
    	element(by.model('credentials.email')).sendKeys('dilip.rai@nextgenpms.com');
        element(by.model('credentials.password')).sendKeys(121);
        element(by.buttonText('Sign In')).click().then(function(){
          console.log('logged in for first approver account for reject the response');
        });
        browser.sleep(5000);
    });
    it("Should able to reject the response ", function () {
        var ele=element.all(by.cssContainingText('.ng-binding','Approve multiApprover - Basic Info')).get(1);
        browser.executeScript("arguments[0].scrollIntoView();", ele.getWebElement());
        ele.click();
        element(by.cssContainingText('.ng-binding','multiApprover Testing')).click();
        element(by.name('approver_comments')).sendKeys('Complete the name for last question');
        element(by.buttonText('Reject')).click();
    });
    it("Should able to logout successfully after rejection", function () {
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/ul/li[9]/a")).click().then(function(){
          console.log('Logout successfully after rejection');
        });
        browser.sleep(2000);
    });
  // Checking response status for second approver after rejecting it by first approver.  
    it("Should able to login to second approver account", function () {
      element(by.model('credentials.email')).sendKeys('srirang.ranjalkar@nextgenpms.com');
        element(by.model('credentials.password')).sendKeys(1234);
        element(by.buttonText('Sign In')).click().then(function() {
          console.log("Logged in successfully to response's creater account");
          browser.sleep(5000);
        });
    });
    it("Should able to check the status response as rejected ", function () {
      var ele=element.all(by.cssContainingText('.ng-binding','Approve multiApprover - Basic Info')).get(1);
        browser.executeScript("arguments[0].scrollIntoView();", ele.getWebElement());
        ele.click();
      var loc=element(by.xpath(".//*[@id='content-main']/p3-loaded/div/div[4]/div/ul/li[./div/div[1]/a ='multiApprover Testing']")).element(by.cssContainingText('.ng-binding','REJECTED'));
         loc.isPresent().then(function(present){
          if(present){
            console.log("Response's status is 'REJECTED' for second approver, which is expected")
          }else{
            console.log("Error:Response status didn't changed for second approver");
          }
        });
    });
    it("Should able to logout successfully after checking rejection status for second approver", function () {
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/ul/li[9]/a")).click().then(function(){
          console.log('Logout successfully after checking rejection status for second approver');
        });
        browser.sleep(2000);
    });
  // Checking response status for third approver after rejecting it by first approver.  
    it("Should able to login to third approver account", function () {
      element(by.model('credentials.email')).sendKeys('sai.kaushik@nextgenpms.com');
        element(by.model('credentials.password')).sendKeys('kaushik123');
        element(by.buttonText('Sign In')).click().then(function() {
          console.log("Logged in successfully to response's creater account");
          browser.sleep(5000);
        });
    });
    it("Should able to check the status response as rejected for third approver", function () {
      element(by.buttonText("Expand All")).click();
      var ele=element.all(by.cssContainingText('.ng-binding','Approve multiApprover - Basic Info')).get(1);
        browser.executeScript("arguments[0].scrollIntoView();", ele.getWebElement());
        ele.click();
      var loc=element(by.xpath(".//*[@id='content-main']/p3-loaded/div/div[4]/div/ul/li[./div/div[1]/a ='multiApprover Testing']")).element(by.cssContainingText('.ng-binding','REJECTED'));
         loc.isPresent().then(function(present){
          if(present){
            console.log("Response's status is 'REJECTED' for third approver, which is expected")
          }else{
            console.log("Error:Response status didn't changed for third approver");
          }
        });
    });
    it("Should able to logout successfully after checking rejection status for third approver", function () {
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/ul/li[9]/a")).click().then(function(){
          console.log('Logout successfully after checking rejection status for second approver');
        });
        browser.sleep(2000);
    });
  //should able to change the entered data  by approver and re-submit it  
    it("Should able to login to response creater account", function () {
      element(by.model('credentials.email')).sendKeys('dilipkumar10cse21@gmail.com');
        element(by.model('credentials.password')).sendKeys(121);
        element(by.buttonText('Sign In')).click().then(function() {
          console.log("Logged in successfully to response's creater account");
          browser.sleep(5000);
        });
    });
    it("Should able to check the status response as rejected ", function () {
      var ele=element.all(by.cssContainingText('.ng-binding','Create multiApprover - Basic Info')).get(1);
        browser.executeScript("arguments[0].scrollIntoView();", ele.getWebElement());
        ele.click();
        browser.sleep(3000);
      var loc=element(by.xpath(".//*[@id='content-main']/p3-loaded/div/div[4]/div/ul/li[./div/div[1]/a ='multiApprover Testing']")).element(by.cssContainingText('.ng-binding','REJECTED'));;
         loc.isPresent().then(function(present){
          if(present){
            console.log("Response's status is 'REJECTED', which is expected")
          }else{
            console.log("Error:Response status didn't changed");
          }
        });
    });

    it("Should able to re-submit the data after editing it", function () {
        element(by.cssContainingText('.ng-binding','multiApprover Testing')).click();
        element(by.name('satyameva_jayate')).sendKeys('Mundak Upanishad');
        element(by.buttonText('Submit')).click().then(function() {
          console.log("Response re-submitted after getting rejected from first approver");
        });
    }); 
    it("Should able to logout successfully", function () {
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/ul/li[9]/a")).click().then(function(){
          console.log('Logout successfully after checking rejection status');
        });
        browser.sleep(2000);
    });


  // Now Approving the response by first approver.
    it("Should able to login to first approver account to approve the response ", function () {
      element(by.model('credentials.email')).sendKeys('dilip.rai@nextgenpms.com');
        element(by.model('credentials.password')).sendKeys(121);
        element(by.buttonText('Sign In')).click().then(function(){
          console.log('logged into first approver account for reject the response');
        });
        browser.sleep(5000);
    });
    it("Should able to approve the response ", function () {
        var ele=element.all(by.cssContainingText('.ng-binding','Approve multiApprover - Basic Info')).get(1);
        browser.executeScript("arguments[0].scrollIntoView();", ele.getWebElement());
        ele.click();
        element(by.cssContainingText('.ng-binding','multiApprover Testing')).click();
        element(by.buttonText('Approve')).click().then(function(){
          console.log("First approver approved the task")
        })
         browser.sleep(3000);
    });
    it("Should able to logout successfully after Approval", function () {
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/ul/li[9]/a")).click().then(function(){
          console.log('Logout successfully after Approval');
        });
        browser.sleep(2000);
    });

  // Now Approving the response by second approver.
    it("Should able to login to second approver account to approve the response ", function () {
      element(by.model('credentials.email')).sendKeys('srirang.ranjalkar@nextgenpms.com');
        element(by.model('credentials.password')).sendKeys(1234);
        element(by.buttonText('Sign In')).click().then(function(){
          console.log('logged into second approver account for approving the response');
        });
        browser.sleep(5000);
    });
    it("Should able to approve the response ", function () {
        var ele=element.all(by.cssContainingText('.ng-binding','Approve multiApprover - Basic Info')).get(1);
        browser.executeScript("arguments[0].scrollIntoView();", ele.getWebElement());
        ele.click();
        element(by.cssContainingText('.ng-binding','multiApprover Testing')).click();
        element(by.buttonText('Approve')).click().then(function(){
          console.log("Second approver approved the task")
        })
         browser.sleep(2000);
    });
    it("Should able to logout successfully after Approval", function () {
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
        element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/ul/li[9]/a")).click().then(function(){
          console.log('Logout successfully after Approval');
        });
        browser.sleep(2000);
    });

  // Now Approving the response by third approver.
    it("Should able to login to third approver account to approve the response ", function () {
      element(by.model('credentials.email')).sendKeys('sai.kaushik@nextgenpms.com');
        element(by.model('credentials.password')).sendKeys('kaushik123');
        element(by.buttonText('Sign In')).click().then(function(){
          console.log('logged into third approver account for approving the response');
        });
        browser.sleep(5000);
    });
    it("Should able to approve the response ", function () {
      element(by.buttonText("Expand All")).click();
        var ele=element.all(by.cssContainingText('.ng-binding','Approve multiApprover - Basic Info')).get(1);
        browser.executeScript("arguments[0].scrollIntoView();", ele.getWebElement());
        ele.click();
        element(by.cssContainingText('.ng-binding','multiApprover Testing')).click();
        element(by.buttonText('Approve')).click().then(function(){
          console.log("Third approver approved the task");
          console.log('response status should changed to submitted, once it got approval from third approver');
        });
    });
    it("should able to go back to task page ",function(){
        element(by.xpath("//*[@id='nav']/li[1]/a")).click();
    });
});

