// var util = require('util')

var Login_Page = require('./Login_Page.js')
var testData = require('./Login_Page_Data.json');
// var log4jsGen = require("./log4js.js");
// var logger = log4jsGen.getLogger();




describe('Login page testcases', function() {

    beforeAll(function(done) {
        jasmine.DEFAULT_TIMEOUT_INTERVAL= 120000;
        done();
    });
    beforeEach(function(done) {

        // jasmine.DEFAULT_TIMEOUT_INTERVAL = 130000;

        //browser.logger.info("Testing Log4js");

        Login_Page.navigateToLoginPage();
        done();

    });


    it('should be able to login using valid Email ID and Password and clicking on Sign In button', function() {
   
        Login_Page.enterEmailAddress(testData[0].emailID);

        //FunctionLibrary.logger.info('Navigating to1 /');

        Login_Page.enterPassword(testData[0].password);

        // expect(Login_Page.getEnteredEmailAddress()).toEqual(testData[0].emailID);

        Login_Page.clickOnSignIn();

        expect(browser.getTitle()).toEqual(testData[3].LoginPageTitle);

    });
});

xdescribe('Login page testcases', function() {

    it('should be able to login using valid Email ID and Password and clicking on enter button', function() {


        Login_Page.enterEmailAddress(testData[0].emailID);

        Login_Page.enterPassword(testData[0].password);

        Login_Page.pressEnterKey();

        expect(browser.getTitle()).toEqual(testData[3].LoginPageTitle);

    });

    it('should not be able to login with incorrect email ID and valid password', function() {

        Login_Page.enterEmailAddress(testData[1].emailID);

        Login_Page.enterPassword(testData[0].password);

        Login_Page.clickOnSignIn();

        expect(Login_Page.getEmailFieldErrorBColor()).toEqual(testData[2].colorForInvalidInput);
    });

    it('should not be able to login using invalid Email ID and empty Password', function() {

        Login_Page.enterEmailAddress(testData[1].emailID);

        Login_Page.clickOnSignIn();

        expect(Login_Page.getEmailFieldErrorBColor()).toEqual(testData[2].colorForInvalidInput);
        

    });

    it('should not be able to login using empty Email ID and valid Password', function() {

        Login_Page.enterPassword(testData[0].password);

        Login_Page.clickOnSignIn();

        expect(Login_Page.getEmailFieldErrorBColor()).toEqual(testData[2].colorForInvalidInput);
    });

    it('should not be able to login using empty Email ID and empty Password', function() {

        Login_Page.clickOnSignIn();

        expect(Login_Page.getEmailFieldErrorBColor()).toEqual(testData[2].colorForInvalidInput);

        expect(Login_Page.getPasswordFieldErrorBColor()).toEqual(testData[2].colorForInvalidInput);

    });

    xit('should not be able to login using invalid Email ID and invalid Password', function() {

        Login_Page.enterEmailAddress(testData[1].emailID);

        Login_Page.enterPassword(testData[1].password);

        Login_Page.clickOnSignIn();

        expect(Login_Page.getErrorText()).toEqual('Sorry, invalid credentials!');

    });

    it('should able to got to nextgen home page clicking on powered by nextgen ', function() {


        expect(Login_Page.getLinkAddress()).toEqual('http://www.nextgenpms.com/');

    });




    afterEach(function() {

        //Login_Page.deleteAllCookies();

        //clear session
        browser.executeScript('window.sessionStorage.clear();');

        //clear local storage
        browser.executeScript('window.localStorage.clear();');

    });
});

xdescribe('Login page testcases', function() {

    beforeEach(function() {

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 90000;

        //browser.logger.info("Testing Log4js");

        Login_Page.navigateToLoginPage();
        Login_Page.enterEmailAddress(testData[0].emailID);
        Login_Page.enterPassword(testData[0].password);
        Login_Page.clickOnSignIn();


    });


    xit('should be able to select the required organization', function() {

        Select_Organization.selectOrganization();

        expect(browser.getTitle()).toEqual('p3 by NextGen - CSR & Development Capital Management Platform');



    });

    xit('should be able to click on Create Event', function() {

        Select_Organization.selectOrganization();

        create_event.clickOnCreateEvent();


        expect(browser.getCurrentUrl()).toEqual('https://demo.p3fy.com/#/volunteeringAdmin/event');


    });

    afterEach(function() {

        //Login_Page.deleteAllCookies();

        //clear session
        browser.executeScript('window.sessionStorage.clear();');

        //clear local storage
        browser.executeScript('window.localStorage.clear();');

    });
});
