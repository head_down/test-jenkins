var chakram = require('chakram'),
    expect = chakram.expect;
describe("Programs API", function () {
  var putID=0;
  this.timeout(50000);
  it("should create a program with name 'apiProgramTest'", function () {
    var response = chakram.post("https://demoapi.p3fy.com:443/api/programs?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa",{
      "name":"apiProgramTest1"
       });
    expect(response).to.have.status(200);
    expect(response).to.have.header("content-type", "application/json; charset=utf-8");
    expect(response).not.to.be.encoded.with.gzip;
    //expect(request).to.have.responsetime(2000);
    return chakram.wait();
  });  
  it("should validate created program with its name", function () {
    return chakram.get("https://demoapi.p3fy.com:443/api/programs?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa").then(function (data) {
      // var name = data.body[4].name;
      // console.log("data:",data);
      var result = null;
      for (var i = 0; i < data.body.length; i++) { 
        if (data.body[i].name === "apiProgramTest1") {   // not using loops because it misguide regarding actual time taken by api
          result = data.body[i];
          break;
        } 
      }Login_Page.
      expect(data).to.have.status(200);
      expect(data).to.have.header("content-type", "application/json; charset=utf-8");
      expect(data).not.to.be.encoded.with.gzip;
      expect(result.name).to.contain("apiProgramTest1");
      // expect(name).to.contain("apiProgramTest1");
      putID=result.id;
      console.log("Programid",putID);
    });
  });
  it("should change the name of a created program to apiProgramTest", function () {
    var response = chakram.put("https://demoapi.p3fy.com:443/api/programs?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa",{
      "name":"apiProgramTest","id":putID
       });
    expect(response).to.have.status(200);
    expect(response).to.have.header("content-type", "application/json; charset=utf-8");
    expect(response).not.to.be.encoded.with.gzip;
    return chakram.wait();
  });  
  it("should get the data of a program by its id",function(){
    var response=chakram.get("https://demoapi.p3fy.com:443/api/programs/"+putID+"?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa"); 
    expect(response).to.have.status(200);
    return chakram.wait();
  })
  it("should check the existance of a program by its id",function(){
    return chakram.get("https://demoapi.p3fy.com:443/api/programs/"+putID+"/exists?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa").then(function (data) {
      expect(data).to.have.status(200);
      expect(data.body.exists).to.equal(true);
    });
  })
  //flow testing
  it("should create a flow for program with name 'First Flow'", function () {
    var response = chakram.post("https://demoapi.p3fy.com:443/api/programs/"+putID+"/flows?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa",{
      "name":"First Flow"
      });
    expect(response).to.have.status(200);
    expect(response).to.have.header("content-type", "application/json; charset=utf-8");
    expect(response).not.to.be.encoded.with.gzip;
    return chakram.wait();
  }); 
  it("should create another flow for program with name 'Second Flow'", function () {
    var response = chakram.post("https://demoapi.p3fy.com:443/api/programs/"+putID+"/flows?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa",{
      "name":"Second Flow"
      });
    expect(response).to.have.status(200);
    expect(response).to.have.header("content-type", "application/json; charset=utf-8");
    expect(response).not.to.be.encoded.with.gzip;
    return chakram.wait();
  }); 
  it("should get flow information by program id",function(){
    return chakram.get("https://demoapi.p3fy.com:443/api/programs/"+putID+"/flows?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa").then(function (data) {
      expect(data).to.have.status(200);
      expect(data.body[0].name).to.contain("First Flow");
    });
  }) 
  it("should count the number of flows in given program",function(){
    return chakram.get("https://demoapi.p3fy.com:443/api/programs/"+putID+"/flows/count?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa").then(function (data) {
      expect(data).to.have.status(200);
      expect(data.body.count).to.equal(2);
    });
  }) 
  it("should delete all created flow for given program id", function () {
    var response=chakram.delete("https://demoapi.p3fy.com:443/api/programs/"+putID+"/flows?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa");
    expect(response).to.have.status(200);
    return chakram.wait();
  });
  //profile testing
  it("should create a profile for program with name 'apiProgram_ProfileTest'", function () {
    var response = chakram.post("https://demoapi.p3fy.com:443/api/programs/"+putID+"/profiles?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa",{
      "name":"apiProgram_ProfileTest1"
      });
    expect(response).to.have.status(200);
    expect(response).to.have.header("content-type", "application/json; charset=utf-8");
    expect(response).not.to.be.encoded.with.gzip;
    return chakram.wait();
  }); 
  it("should create another profile for program with name 'apiProgram_ProfileTest2'", function () {
    var response = chakram.post("https://demoapi.p3fy.com:443/api/programs/"+putID+"/profiles?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa",{
      "name":"apiProgram_ProfileTest2"
      });
    expect(response).to.have.status(200);
    expect(response).to.have.header("content-type", "application/json; charset=utf-8");
    expect(response).not.to.be.encoded.with.gzip;
    return chakram.wait();
  }); 
  it("should get profile information by program id",function(){
    return chakram.get("https://demoapi.p3fy.com:443/api/programs/"+putID+"/profiles?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa").then(function (data) {
      expect(data).to.have.status(200);
      expect(data.body[0].name).to.contain("apiProgram_ProfileTest1");
    });
  }) 
  it("should count the number of profiles in given program",function(){
    return chakram.get("https://demoapi.p3fy.com:443/api/programs/"+putID+"/profiles/count?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa").then(function (data) {
      expect(data).to.have.status(200);
      expect(data.body.count).to.equal(2);
    });
  }) 
  it("should delete all created profiles for given program id", function () {
    var response=chakram.delete("https://demoapi.p3fy.com:443/api/programs/"+putID+"/profiles?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa");
    expect(response).to.have.status(200);
    return chakram.wait();
  });  
  //project testing
  it("should create a project for program with name 'apiProgram_ProjectTest'", function () {
    var response = chakram.post("https://demoapi.p3fy.com:443/api/programs/"+putID+"/projects?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa",{
      "name":"apiProgram_ProjectTest"
      });
    expect(response).to.have.status(200);
    expect(response).to.have.header("content-type", "application/json; charset=utf-8");
    expect(response).not.to.be.encoded.with.gzip;
    return chakram.wait();
  }); 
  it("should get project information by program id",function(){
    return chakram.get("https://demoapi.p3fy.com:443/api/programs/"+putID+"/projects?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa").then(function (data) {
      expect(data).to.have.status(200);
      expect(data.body[0].name).to.contain("apiProgram_ProjectTest");
    });
  }) 
  it("should count the number of profiles in given program",function(){
    return chakram.get("https://demoapi.p3fy.com:443/api/programs/"+putID+"/projects/count?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa").then(function (data) {
      expect(data).to.have.status(200);
      expect(data.body.count).to.equal(1);
    });
  }) 
  it("should delete all created projects for given program id", function () {
    var response=chakram.delete("https://demoapi.p3fy.com:443/api/programs/"+putID+"/projects?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa");
    expect(response).to.have.status(200);
    return chakram.wait();
  }); 
  //Tag testing
 it("should create a tag for program with name 'state'", function () {
    var response = chakram.post("https://demoapi.p3fy.com:443/api/programs/"+putID+"/tags?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa",{
      "name":"state", "levelLabels": ["Arunachal Pradesh","Assam","Bihar","Karnataka"]
      });
    expect(response).to.have.status(200);
    expect(response).to.have.header("content-type", "application/json; charset=utf-8");
    expect(response).not.to.be.encoded.with.gzip;
    return chakram.wait();
  }); 
  it("should create another tag for program with name 'city'", function () {
    var response = chakram.post("https://demoapi.p3fy.com:443/api/programs/"+putID+"/tags?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa",{
      "name":"city", "levelLabels": ["Itanagar","Guahati","Muzaffarpur","Bangalore"]
      });
    expect(response).to.have.status(200);
    expect(response).to.have.header("content-type", "application/json; charset=utf-8");
    expect(response).not.to.be.encoded.with.gzip;
    return chakram.wait();
  }); 
  it("should get tag information by program id",function(){
    return chakram.get("https://demoapi.p3fy.com:443/api/programs/"+putID+"/tags?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa").then(function (data) {
      expect(data).to.have.status(200);
      expect(data.body[0].name).to.contain("state");
    });
  }) 
  it("should count the number of tags in given program",function(){
    return chakram.get("https://demoapi.p3fy.com:443/api/programs/"+putID+"/tags/count?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa").then(function (data) {
      expect(data).to.have.status(200);
      expect(data.body.count).to.equal(2);
    });
  }) 
  it("should delete all created projects for given program id", function () {
    var response=chakram.delete("https://demoapi.p3fy.com:443/api/programs/"+putID+"/tags?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa");
    expect(response).to.have.status(200);
    return chakram.wait();
  }); 
  //Creating users
  it("should add an user with dataentry role", function () {
    var response = chakram.post("https://demoapi.p3fy.com:443/api/programs/createProgramUser?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa",{
      "programId":putID,"email":"dilip.rai@nextgenpms.com","role":"DATAENTRY"
    });
    expect(response).to.have.status(200);
    expect(response).to.have.header("content-type", "application/json; charset=utf-8");
    expect(response).not.to.be.encoded.with.gzip;
    return chakram.wait();
  }); 
  it("should count the number of programs available for logged in user",function(){
    return chakram.get("https://demoapi.p3fy.com:443/api/programs/count?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa").then(function (data) {
      expect(data).to.have.status(200);
      expect(data.body.count).to.equal(5);
    });
  }) 

  //Deleting created program
  it("should delete the created program named apiProgramTest", function () {
    var response=chakram.delete("https://demoapi.p3fy.com:443/api/programs/"+putID+"?access_token=QK4UGeaiYtsq7tMWMiydYO74CMCBQE9ma4p0rD2wsdcsViPg6HEnYiLM9MSiNmCa");
    expect(response).to.have.status(200);
    return chakram.wait();
  });

}); 

