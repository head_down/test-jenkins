describe('Protractor Demo App', function() {
    it('Should navigate to "programs" page', function() {
        browser.sleep(7000);
    	element(by.xpath("//*[@id='nav']/li[2]/a/table/tbody/tr/td[2]")).click().then(function(){
        expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/programs');
        console.log("Navigated to programs page successfully");
    	})
    });
    it('Should navigate to "Task" page back', function() {
    	element(by.xpath("//*[@id='nav']/li[1]/a/table/tbody/tr/td[2]")).click().then(function(){
        expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/tasks');
        console.log("Navigated to task page successfully");
    	})
    });
    it('Should navigate to "Home" page', function() {
    	element(by.xpath("//*[@id='header']/div/nav/div/div[1]/ul/li[1]/a")).click();
    	element(by.xpath("//*[@id='header']/div/nav/div/div[1]/ul/li[1]/ul/li/a")).click().then(function(){
        expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/home');
        console.log("Navigated to Home page successfully");
    	})
    });
    it('Should navigate to "task" page from "Home" page', function() {
    	element(by.xpath("//*[@id='header']/div/nav/div/div[1]/ul/li[1]/a/small")).click();
    	element(by.xpath("//*[@id='header']/div/nav/div/div[1]/ul/li[1]/ul/li/a")).click().then(function(){
        expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/tasks');
        console.log("Navigated to task page successfully");
    	})
    });
    it('should able to collapse all task names under project', function() {   
        browser.sleep(2000);
        element(by.buttonText('Collapse All')).click(); 
        element.all(by.css('a[href*="#/task/31459"]')).get(0).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element doesn't exist which is expected, collapse functionality is working");
              }
        });
    });
    it('should able to expand all task names under project', function() {   
        element(by.buttonText('Expand All')).click(); 
        element.all(by.css('a[href*="#/task/31459"]')).get(0).isDisplayed().then(function(present) {
            if (present) {
                console.log("element exist which is expected, expand functionality is working");
              } else {
                 console.log("Error:element doesn't exists which is not expected.");
              }
        });
    });
    it('Should able to check "add program" cancle button', function() {
    	element(by.xpath("//*[@id='topnav']/ul/li[2]/a")).click();
    	var ele=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/p3-list/div/div/button"));
    	browser.executeScript('window.scrollTo(0,1000);').then(function () {
           ele.click();
         });
    	element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/p3-list/div/div/form/button[2]")).click().then(function(){
        console.log("Cancle button worked properly");
    	})
    });
});
