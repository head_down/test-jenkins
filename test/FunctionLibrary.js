var function_library = function() {

    var log4jsGen = require("./log4js.js");
    var logger = log4jsGen.getLogger();
    var EC = protractor.ExpectedConditions;
    var initialScrollValue=0;
    var finalScrollValue=1500;





    this.getAttributeValue = function(locator, attribute, name) {

        logger.info('Getting the ' + attribute + ' attribute of ' + name + ' field');

        return locator.getAttribute(attribute);


    };

    /**
     * click(locator, elemName) method
     * specification :-
     * 
     * 1) Click and wait for next page to load 2)
     * driver.findElement(locator).click() -> Clicks on the web element targeted
     * by locator 3) driver.navigate().refresh() -> Refresh the page
     * 
     * @param : Locator to locate the web element, Name of the web element
     * @return : Result of execution - Pass or fail (with cause)
     */

    this.click = function(locator, name) {

        logger.info('Clicking on ' + name + ' field');

        locator.click().then(function(){

          logger.info('Clicked on ' + name + ' field');
        });
    };

    this.scrollClick = function(locator, name) {

        browser.executeScript("window.scrollTo('initialValue','finalValue');");
        this.click(locator,name);

    };

    /**
     * sendKeys(locator, value, name) method
     * method specification :-
     * 
     * 1) Inputs/sends value 2) locator -> identify the web element by
     * id,x-path,name,etc. 3) elemName -> the name of the web element where we
     * intend to input/send values 4) Value -> the string value which we intend
     * to input/send 5) waitForElementToLoad(locator) -> waits for web element
     * to load 6) driver.findElement(locator).sendKeys(Value) -> inputs/sends
     * the value to the intended web element
     * 
     * @param : Locator for the input-box, value to be
     *        inputted, name of the web element
     * @return : Result of execution - Pass or fail (with cause)
     */


    this.sendKeys = function(locator, value, name) {

        logger.info('Sending input value to ' + name + ' field');

        locator.sendKeys(value).then(function(){
  
           logger.info('Sent input value to ' + name + ' field');
        });
    };


    this.getText = function(locator, name) {

        logger.info('Getting text from ' + name + ' field');

        return locator.getText();

    };
    this.waitThengetText = function(locator,name,milisec) {

        logger.info('Getting text from ' + name + ' field');

        browser.wait(EC.elementToBeClickable(locator),milisec);

        return locator.getText();

    };

    // this.uploadFile = function() {

    //     var fileToUpload = 'C:\Users\NEXTGEN\Downloads\images1.jpg',

    //         var absolutePath = path.resolve(__dirname, fileToUpload);
    //     $('input[type="file"]').sendKeys(absolutePath);
    // }


    // this.


};
module.exports = new function_library();
