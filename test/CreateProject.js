var dragNDrop = require('./drag.js');
var locator=require("./Locators.js").config;
var EC = protractor.ExpectedConditions;
describe('Project Creation:', function() {
    var name=element(by.name("name"));
    it("Should navigate to program page", function () {
        element(by.xpath(locator.Programs)).click();	
    });
    it("should able to create a program for project testing ", function () {
      element(by.xpath(locator.AddEntry)).click();
      browser.sleep(2000);
      name.sendKeys("TestProject_Program").then(function(){
        element(by.buttonText("Save")).click();
        browser.sleep(2000);
      })
    })
    it("should able to navigate to project page", function () {
      element(by.cssContainingText(".ng-binding.ng-scope","TestProject_Program")).click();  
	  element(by.xpath(locator.Projects)).click();
    })
    it("should able to close the pop-up once clicked on 'close' button", function () {
      // element(by.xpath(locator.CreateProject)).click();
      element(by.buttonText("Create First Project")).click();
      browser.sleep(2000);
      element(by.cssContainingText(".btn.btn-default",'Close')).click();
      browser.sleep(2000);
    })
    it("should not save project without name", function () {
      element(by.xpath(locator.CreateProject)).click();
      browser.sleep(2000);
      element(by.buttonText("Save")).click();
      browser.sleep(2000);  
    });
    it("should able to verify that project name is mandatory", function () {
      var ele=element(by.css('.form-control.ng-invalid.ng-invalid-required')); // ng-pristine is getting deleted on html code once inputbox changes to pink color, i.e, trying to save without any name.
          ele.isDisplayed().then(function(present) {
            if (present) {
                console.log("Check Sucessful not alloying to save project without name.");
              } else {
                 console.log("Error: Save element not changing to pink color.");
              }
           });
    })
    it("should able to create a project", function () {
        browser.sleep(2000);
        element(by.name("name")).sendKeys("Project_testing");
        element(by.buttonText('Save')).click().then(function(){
        console.log("project created successfully");
        browser.sleep(2000);
        })
    });  
    it("should able to change the project name", function () {
        element(by.xpath("//*[@id='tree-root']/li/div/div/div[2]/a[1]/span")).click();
        // element(by.xpath("//*[@id='tree-root']/li/div/div/div[2]/a[1]/span")).click();
        browser.sleep(2000);
        element(by.name("name")).clear().then(function(){
            element(by.name("name")).sendKeys("Project_Testing");
        })
        element(by.buttonText("Save")).click();
        browser.sleep(4000);
    });   
});


// describe('Flows and profile Creation::', function() {
    // it("should able to navigate to project page", function () {
    //   element(by.xpath(locator.Programs)).click();
    //   element(by.cssContainingText(".ng-binding.ng-scope","TestProject_Program")).click();  
    //   element(by.xpath(locator.Projects)).click();
    // })
//     it("should able to navigate to Profile page", function () {
//       element(by.xpath(locator.Programs)).click();
//       element(by.cssContainingText(".ng-binding.ng-scope","TestProject_Program")).click();  
//       element(by.xpath(locator.Profiles)).click();
//     })
//     it("should create a new Profile", function () {
//         element(by.buttonText('Create First Profile')).click();
//         element(by.name("name")).sendKeys("ProjectTest_profile");
//         element(by.buttonText('Save')).click(); 
//         browser.sleep(2000);
//     });
//     it("should navigate to flows page", function () { 
//         element(by.xpath(locator.Flows)).click();
//     });
//     it("should close the pop-up on flows page", function () {
//         element(by.buttonText("Create First Flow")).click();
//         browser.sleep(1000);
//         element(by.buttonText("Close")).click();
//         browser.sleep(2000);
//     }); 
//     it("should create a new flow", function () {
//         element(by.buttonText("Create First Flow")).click();
//         element(by.name("name")).sendKeys("prject_Flow");
//         element(by.buttonText("Save")).click();
//     });
//     it("should create flow with AddEntry button", function () {
//         element(by.buttonText("ADD Entry")).click();
//         element(by.name("name")).sendKeys("FlowAdd");
//         element(by.buttonText("Save")).click();
//     });
//     it("should create flow with add button", function () { 
//         element(by.xpath("locator.plusFlow")).click();
//         element(by.name("name")).sendKeys("FlowPlus");
//         element(by.buttonText("Save")).click();
//     });
//     it("should delete the created flow", function () {
//         element(by.cssContainingText(".ng-binding.ng-scope","FlowPlus")).element(by.css('.glyphicon.glyphicon-remove')).click();
//         browser.switchTo().alert().accept();
//     }); 
//     it("should cancel the name change activity for flow", function () {
//         element(by.cssContainingText(".ng-binding.ng-scope","projectFlow")).element(by.css('.glyphicon.glyphicon-pencil')).click();
//         element(by.xpath(locator.flowname)).click();
//         element(by.xpath("locator.flowCross")).click();
//     });
//     it("should edit the created flow to change the flow name", function () { 
//        element(by.xpath(locator.flowname)).click();
//        element(by.xpath(locator.flownameInput)).sendKeys("projectFlow");
//        element(by.xpath(locator.flowTick)).click();

//     });
//     it("should cancel the name change activity for phase", function () {
//         element(by.buttonText("Phase")).click();
//         element(by.cssContainingText(".ng-binding","New Phase")).click(); 
//         element(by.xpath(locator.phaseCross)).click();
//     });
//     it("should create new phase on flow page", function () {
//        element(by.cssContainingText(".ng-binding","New Phase")).click(); 
//        element(by.xpath(locator.phaseNameInput)).sendKeys("First Quarter delete");
//        element(by.xpath(locator.phaseTick)).click();
//     }); 
//     it("should delete the created phase on flow page", function () {
//         element(by.xpath(locator.phaseDelete)).click();
//     });
//     it("should create new phase on flow page second time", function () {
//        element(by.cssContainingText(".ng-binding","New Phase")).click(); 
//        element(by.xpath(locator.phaseNameInput)).sendKeys("First Quarter");
//        element(by.xpath(locator.phaseTick)).click();
//     }); 
//     it("should drag the profile segment to created phase", function () {
//         var source=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/div[2]/div[2]/div[3]/div/ul/li"));
//         var des=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/div[1]/div/ul"));
//         browser.executeScript(dragNDrop,source.getWebElement(),des.getWebElement());
//         browser.sleep(2000);
//     });    
  
// });



// describe('Task assignment to User', function() {
//     it("should go back to projects page, then open the created project", function () {
//         element(by.xpath(locator.Projects)).click();
//         element(by.cssContainingText("Project_Testing")).click();
//     })   
//     it("should able to set project date", function () {  
//         element(by.cssContainingText(".ng-binding","projectTest_profile - Basic Info")).click();
//         element(by.name("startDate")).sendKeys('09/19/2016');
//         element(by.name("endDate")).sendKeys('09/19/2017');
//         element(by.buttonText('Save')).click().then(function(){
//         console.log("Project date seted successfully")
//     });
//         browser.sleep(4000);
//     })
//     it("should able to assign project to user",function(){
//         element(by.xpath("//*[@id='tabContainer']/ul/li[2]/a")).click();
//         element(by.xpath("//*[@id='users']/p3-quick-add-user-with-approver/div/div/button")).click();
//         element(by.xpath("//*[@id='userEmailField']")).sendKeys("dilipkumar10cse21@gmail.com");
//         element(by.xpath("//*[@id='approverEmailField']")).sendKeys("dilip.rai@nextgenpms.com");
//         element(by.xpath("//*[@id='users']/p3-quick-add-user-with-approver/div/div/form/table/tbody/tr/td[3]/button[1]")).click().then(function(){
//         console.log("project assigned to dilipkumar10cse21@gmail.com successfully");
//         browser.sleep(4000);
//         });
//     });

// });

// describe("Deleting the created programs named 'TestProject_Program'",function(){
//     it("should able to delete the created project", function () {
//       element(by.xpath(locator.Programs)).click();
//       element(by.cssContainingText(".ng-binding.ng-scope","TestProject_Program")).click(); 
//       element(by.xpath(locator.Projects)).click();
//       var elm=element(by.xpath("//*[@id='tree-root']/li/div/div/div[2]/a[2]/span")); 
//           elm.isPresent().then(function(present) {
//             if (present) {
//                 elm.click().then(function(){
//                     browser.switchTo().alert().accept();    
//                     console.log("Project_Testing is deleted successfully.");
//                 });
//             } else {
//                  console.log("Project_Testing doesn't exist which is expected.");
//               }
//         });
//       browser.sleep(2000);           
//     });
//     it("should able to delete the created program", function () {
//       element(by.xpath(locator.Programs)).click();
//       var elm=element(by.xpath(".//*[@id='content-main']/p3-loaded/div/div/p3-list/ul/li[./div/div/a ='TestProject_Program']")).element(by.css('.glyphicon.glyphicon-remove'));  
//           elm.isPresent().then(function(present) {
//             if (present) {
//                 elm.click().then(function(){
//                     browser.switchTo().alert().accept();    
//                     console.log("TestProject_Program is deleted successfully.");
//                 });
//             } else {
//                  console.log("TestProject_Program doesn't exist which is expected.");
//               }
//         });
//       browser.sleep(2000);           
//     });
//     it("should comeback to Task page", function () {
//     element(by.xpath(locator.MyTasks)).click();          
//     })

// });
