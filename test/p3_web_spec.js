describe('Protractor Demo App', function() {//describing what we want to do
  beforeEach(function() { // means loading the below url and then executing the commands before each it()
    browser.get('https://app.p3fy.com/#/login');// opening url in browser
  });
  var email =element(by.model('credentials.email')); //finding element on page and saving in a variable
  var password = element(by.model('credentials.password'));//finding element on page and saving in a variable
  //var button = element(by.className('btn btn-primary')); //this is a example by class name.
  var login=element(by.buttonText('Sign In'));//finding element on page and saving in a variable 
  it('should login', function() { //this is a function
    email.sendKeys("dilip.rai@nextgenpms.com");//sending keys to textbox,locater we find before
    password.sendKeys(121);//sending keys to textbox,locater we find before

    expect(email.getAttribute('value')).toEqual('dilip.rai@nextgenpms.com');//except function for matching the content of textbox after sending keys.
    expect(password.getAttribute('value')).toEqual('121');
   
    login.click().then(function(){ //clicking on sign In button
    expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/tasks');//checking the page after clicking on login button
    console.log("Loged In Sucessfully");//  printing the console output
    var perlogs=log('performance');
    console.log(perlogs);
   // var link =element(by.cssContainingText("a","Create Help Text Test Profile - Basic Info")).getAttribute('href');
   // link.click();
   // var plus =browser.element(by.xpath(".//*[@id='content-main']/p3-loaded/div/div[1]/div[1]/p3-titlebar/div/div[2]/div[1]/div/a"));
   // plus.click().then(function(){
   //  expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/task/606/response');
   //  console.log("Reach data entry page Sucessfully");
   //  });
    });

  });
  // it('should be performant',function(){
  //   yield driver.get("http://localhost:8080/index.html");
  //   var perlogs=yield driver.log('performance');
  // });


});
