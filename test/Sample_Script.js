
1) creating a locator for diggin an element inside an html element:
    element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/p3-list/ul/li[./div/div/a ='Precommit_Testing']")).element(by.css('.glyphicon.glyphicon-remove'))
2) checking an elements by src:
    expect(element(by.css('img.img-responsive')).getAttribute('src')).toMatch(/bird/);
3) Aleart handling:
    browser.switchTo().alert().dismiss();
    browser.switchTo().alert().accept();
4) using wait condition on protractor:
    var EC = protractor.ExpectedConditions;
    browser.wait(EC.elementToBeClickable(elm), 5000);
    elm.click();
5) To select an element from selectbox.
    it("should able to select value for profile-ref element", function () {
         element(by.name('')).element(by.cssContainingText('option', '')).click().then(function() {
           console.log('school location selected sucessfully');
         });
    });
6) Locating an element by href: 
    element(by.css('a[href="#/task/114190"]')).click();

7)browser.actions().mouseMove(ele).click().perform();

8) scrolling to a particular element:
        browser.executeScript('window.scrollTo(0,1000);').then(function () {
           ele.click();
         })
9) xpath creation by specific text:
    .//*[contains(text(),'Project_testing')]
10) xpath by span
    //span[contains(text(),'Precommit_profile-1 - Basic Info')]
11) Waiting for element to become clickable:
     browser.wait(EC.elementToBeClickable(locator), 10000);
12) waiting for element to be displayed:

  
