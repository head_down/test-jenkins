var home_page = function() {

    //     var log4jsGen = require("../HelperModule/log4js.js");
    // var logger = log4jsGen.getLogger();


    var FunctionLibrary = require('./FunctionLibrary.js')


    var errorModalLocator = element(by.id('errorDialog'));

    var emailIDLocator = element(by.model('credentials.email'));

    var passwordLocator = element(by.model('credentials.password'));

    var signInLocator = element(by.buttonText('Sign In'));

    var poweredByNextGenLocator = element(by.linkText('Powered by NextGen'));

    var nextGenLogoLocator = element(by.css('.row.ng-scope'));

    var EC = protractor.ExpectedConditions;


    var emailIDName = "Email input box";

    var passwordName = "Password input box";

    var poweredByNextGenName = "Powered By Nextgen";

    var signInName = "Sign In";

    this.navigateToLoginPage = function() {

        //browser.ignoreSynchronization = false;

        browser.get('https://demo.p3fy.com/');

        browser.manage().window().maximize(); // To maximize the browser to full screen size.

        browser.waitForAngular(); //To wait for page to be fully loaded.
    };

    this.enterEmailAddress = function(value) {

        FunctionLibrary.sendKeys(emailIDLocator, value, emailIDName);
    };

    this.enterPassword = function(value) {


        FunctionLibrary.sendKeys(passwordLocator, value, passwordName);
    };

    this.clickOnSignIn = function() {

        FunctionLibrary.click(signInLocator, signInName);

    };

    this.pressEnterKey = function() {

        //browser.actions().sendKeys(protractor.key.ENTER).perform();

        var enter = browser.actions().sendKeys(protractor.Key.ENTER);

        enter.perform();
    }

    this.deleteAllCookies = function() {

        browser.driver.manage().getCookies().then(function(cookies) {
            console.log('Got cookies %j', cookies);
        });

        browser.driver.manage().deleteAllCookies();

        //browser.sleep(2000);

        browser.driver.manage().getCookies().then(function(cookies) {
            console.log('Got cookies %j', cookies);
        });
    }

    // this.getEnteredEmailAddress = function() {

    //     return emailIDLocator.getAttribute('value');

    // };
    this.getEnteredEmailAddress = function() {

         return FunctionLibrary.getAttributeValue(emailIDLocator,'value',emailIDName);

    };

    this.getErrorText = function() {


        return browser.executeScript('return (arguments[0].textContent).trim()', errorModalLocator)

    };

    this.getEmailFieldErrorBColor = function() {

        // logger.info('Displayed text is:', emailID.getCssValue('background-color'));

        return emailIDLocator.getCssValue('background-color');

    };

    this.getPasswordFieldErrorBColor = function() {

        return passwordLocator.getCssValue('background-color');

    };

    this.getLinkAddress = function() {

        return FunctionLibrary.getAttributeValue(poweredByNextGenLocator, 'href', poweredByNextGenName);

        //return poweredByNextGen.getAttribute('href');
    };

};
module.exports = new home_page();
