// var ProtractorPerf = require('protractor-perf');
// describe('angularjs homepage todo list', function() {
//     var perf = new ProtractorPerf(protractor); // Initialize the perf runner
//     it('should add a todo', function() {
//         browser.get('https://app.p3fy.com/#/login');

//         perf.start(); // Start measuring the metrics
//           var email =element(by.model('credentials.email')); //finding element on page and saving in a variable
//           var password = element(by.model('credentials.password'));//finding element on page and saving in a variable
//           //var button = element(by.className('btn btn-primary')); //this is a example by class name.
//           var login=element(by.buttonText('Sign In'));//finding element on page and saving in a variable 
//             email.sendKeys("dilip.rai@nextgenpms.com");//sending keys to textbox,locater we find before
//             password.sendKeys(121);//sending keys to textbox,locater we find before

//             // expect(email.getAttribute('value')).toEqual('dilip.rai@nextgenpms.com');//except function for matching the content of textbox after sending keys.
//             // expect(password.getAttribute('value')).toEqual('121');
           
//             // login.click().then(function(){ //clicking on sign In button
//             // expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/tasks');//checking the page after clicking on login button
//             // console.log("Loged In Sucessfully");//  printing the console outp
//             // });
//         perf.stop(); // Stop measuring the metrics 

//         if (perf.isEnabled) { // Is perf measuring enabled ?
//             // Check for perf regressions, just like you check for functional regressions
//             expect(perf.getStats('meanFrameTime')).toBeLessThan(60); 
//         };
//     });
// });


var PerfRunner = require('protractor-perf');
describe('angularjs homepage todo list', function() {
    var perfRunner = new PerfRunner(protractor, browser);
    // var sgpt = require('sg-protractor-tools');

    it('should add a todo', function() {
        browser.get('https://app.p3fy.com/#/login');
        perfRunner.start();

        element(by.model('credentials.email')).sendKeys("dilip.rai@nextgenpms.com");
        element(by.model('credentials.password')).sendKeys(121);
        element(by.buttonText('Sign In')).click();
        perfRunner.stop();

        if (perfRunner.isEnabled) {
            perfRunner.printStats();
            expect(perfRunner.getStats('meanFrameTime')).toBeLessThan(60);
        };

        // var todoList = element.all(by.repeater('todo in todoList.todos'));
        // expect(todoList.count()).toEqual(3);
        // expect(todoList.get(2).getText()).toEqual('write a protractor test');
});

//     it('should increase the memory consumption', function () {
//     var iterations = 250;

//     sgpt.memory.runTestFunction(this, iterations, function () {
//         // This test function will be called 250 times, and the memory is measured after each iteration.
//        element(by.model('credentials.email')).sendKeys("dilip.rai@nextgenpms.com");
//         // Your test code goes here.
//         element(by.model('credentials.email')).clear();
//     });
// }, 100000);
});
