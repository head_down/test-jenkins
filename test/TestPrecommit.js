var dragNDrop = require('./drag.js');
var path = require('path');
describe('Protractor Demo App', function() {
  browser.sleep(5000);
	it("should able to navigate to question page", function () {
        // browser.sleep(7000);
        // var ele=element(by.xpath(".//*[@id='pending']/div[7]/div/li/div/div/div/div[1]/a/span"));
        var ele=element(by.xpath("//span[contains(text(),'Precommit_profile-1 - Basic Info')]"));
         // var ele=element.all(by.css('a[href="#/task/115092]')).get(0);
         // var ele=element.all(by.cssContainingText('.ng-binding.ng-scope','Create  Precommit_profile-1 - Basic Info')).get(0);
             browser.executeScript('window.scrollTo(0,2000);').then(function () {
             ele.click();
            });
        browser.sleep(3000);
    });
    it("should able to input value for textbox element", function () {
              browser.sleep(2000);
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[1]/p3-titlebar/div/div[2]/div[1]/div/a")).click();
       var textbox=element(by.name('test_textbox'));
       textbox.sendKeys('WEB: Web_App_Testing').then(function() {
          expect(textbox.getAttribute('value')).toEqual('WEB: Web_App_Testing');
          console.log('textbox tested successfully');
        });
    });
    it("should able to select value for select element", function () {
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/form/fieldset/div[2]/div/div/p3-input-select/div/div/div/div/div[1]/label/input")).click().then(function() {
          console.log('select-box tested sucessfully');
        });
    });
    // it("should able to select value for profile-ref element", function () {
    //     element(by.name('test_profileref')).element(by.cssContainingText('option', '')).click(); .then(function() {
    //       console.log('input value for textbox is entered sucessfully');
    //     });
    // });
    it('should upload an image file', function() {
      var fileToUpload = '/Users/nextgen/Downloads/Test_Pic/birds.png',
          absolutePath = path.resolve(__dirname, fileToUpload);
          element.all(by.css('input[type="file"]')).get(0).sendKeys(absolutePath).then(function(){
          console.log("Photo element tested successfully")
          browser.sleep(3000);
      });
    });
    it("should able to input value for paragraph element", function () {
        element(by.name('test_paragraph')).sendKeys('This is precommit testing for web application').then(function() {
          console.log('paragraph element tested sucessfully');
        });
    });
    it("should able to input value for number element", function () {
        element(by.name('test_number')).sendKeys(500000).then(function() {
          console.log('Number element tested sucessfully');
        });
    });
    it('should upload a pdf file', function() {
      browser.sleep(5000);
      var fileToUpload = '/Users/nextgen/Downloads/Test.pdf',
          absolutePath = path.resolve(__dirname, fileToUpload);
          browser.executeScript('window.scrollTo(0,1000);').then(function () {
            element.all(by.css('input[type="file"]')).get(1).sendKeys(absolutePath).then(function(){
            console.log("File element tested successfully");
          });
      });
    });
    it("should able to input date for date element", function () {
        element(by.name('test_date')).sendKeys('09/06/2016').then(function() {
          console.log('Date element tested sucessfully');
        });
    });
    it("should able to check toggle element", function () {
        element(by.name('test_toggle')).element(by.cssContainingText('option', 'Yes')).click().then(function(){
          console.log('Toggle element tested sucessfully');
        });
    });
    it("should able to input value for name element", function () {
        element(by.name('name')).sendKeys('Precommit_Automation').then(function() {
          console.log('Name element tested sucessfully');
        });
    });
    it('should click on save button and save the responses', function () {
      element(by.buttonText('Save As Draft')).click();
        browser.sleep(3000);

    });
    it("should able to go back to task page ",function(){
        element(by.xpath("//*[@id='nav']/li[1]/a")).click();
        browser.sleep(3000);
    });
  });
