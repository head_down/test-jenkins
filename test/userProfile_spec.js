describe('User Profile verification', function() {
    it('Should navigate to "user profile " page', function() {
    	element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
    	element(by.xpath("//*[@id='topnav']/ul/li[3]/ul/li[1]/a")).click().then(function(){
        expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/user/profile');
        console.log("Navigated to user's profile page successfully");
    	})
    });
    it("Should Verify User's name and mobile number successfully" , function() {
        var full_name=element(by.name('name'));
        var mobile=element(by.name('mobile'));
        // expect(.getAttribute('value')).toEqual('');
        expect(full_name.getAttribute('value')).toEqual('Dilip KR Gmail');
        expect(mobile.getAttribute('value')).toEqual('9538856298');
    });
    it('Should able to change the mobile number and save it successfully' , function() {
        var mobile=element(by.name('mobile'));
        mobile.clear().then(function() {
          mobile.sendKeys('9035896405');
        element.all(by.buttonText('Update')).get(0);
        })
    });
    it('Should verify the updated number successfully and change it to previous number' , function() {
        expect(mobile.getAttribute('value')).toEqual('9035896405');
        var mobile=element(by.name('mobile'));
        mobile.clear().then(function() {
          mobile.sendKeys('9538856298');
        element.all(by.buttonText('Update')).get(0);
        })
    });
    it('Should able to upload a picture successfully' , function() {
        expect(element(by.css('img.img-responsive')).getAttribute('src')).toMatch(/bird/);
    });
    it('Should able to change a picture successfully' , function() {
    });
    it('Should able to select a skills form drop down ' , function() {
       var skill=element(by.name('skill'));
       expect(skill.getAttribute('value')).toEqual('IT');
       skill.element(by.cssContainingText('option', 'IT')).click(); 
    });
    it('Should able to select area of interest form drop down ' , function() {
        element(by.name('skill')).element(by.cssContainingText('option', 'IT')).click(); 
    });
    it('Should navigate to "security" page', function() {
    	element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
    	element(by.xpath("//*[@id='topnav']/ul/li[3]/ul/li[2]/a")).click().then(function(){
        expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/user/security');
        console.log("Navigated to user's security page successfully");
    	})
    });
    it('Should able to enter new password and save it ' , function() {
    });
    it('Should not allowed to save without entering any data in password field ' , function() {
    });
    // code for changing the password 

    it('Should navigate to "organizations" page', function() {
        //span missing
    	element(by.xpath("//*[@id='topnav']/ul/li[3]/a/")).click();
    	element(by.xpath("//*[@id='topnav']/ul/li[3]/ul/li[5]/a")).click().then(function(){
        expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/organizations');
        console.log("Navigated to organizations page successfully");
    	})
    });
    it('Should navigate to "Setting" page' , function() {
    });
    it('Should able to select preffered theme from drop down ' , function() {
    });
    it('Should able to customize notifications, users want to receive ' , function() {
    });
    it('Should able to "sign out" successfully', function() {
    	element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/a/small")).click();
    	element(by.xpath("//*[@id='leftnav-container']/div[1]/ul/li/ul/li[9]/a")).click().then(function(){
        expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/login');
        console.log("User logout successfully");
    	})
    });

});