describe('Testing all types of display Logic functionality:', function() {
	it('We are navigating to "response" page', function() {
        browser.sleep(7000);
        var ele=element.all(by.css('a[href*="#/task/31540"]')).get(0);
        browser.executeScript('window.scrollTo(0,1000);').then(function () {
           ele.click();
         });
        element(by.xpath("//*[@id='content-main']/p3-loaded/div/div[1]/p3-titlebar/div/div[2]/div[1]/div/a")).click();
	});
	it('Should insert name for web display Logic Testing:', function() {   
        element(by.name('name')).sendKeys("Web based display Logic Testing").then(function() {
        console.info("Validation name entered succesfully");
        });
    });
    it('Checking "contains" logic for textbox at invisible point', function() { 
        element(by.name('toggle_contains')).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element for 'contains' logic doesn't exist which is expected.");
              }
        });
    }); 
    it('Entering value for "contains" logic:', function() {   
        element(by.name('text_contains')).sendKeys('my company is nextgen').then(function(text) {
            console.info("Value entered in text field for checking 'contains' functionality ");
        });
    });
    it('Checking "contains" logic for textbox at visible point', function() {
        browser.sleep(2000);
        var contains=element(by.name('toggle_contains')); 
        contains.isDisplayed().then(function(present) {
            if (present) {
                browser.actions().mouseMove(contains).click().perform();
                console.log("element exists and test is successfully passed");
              } else {
                 console.log("Error: element doesn't exist");
              }

        });
    });
    it('Checking "begins with" logic for textbox at invisible point', function() { 
        element(by.name('toggle_bw')).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element for 'begins with' logic doesn't exist which is expected.");
              }
        });
    }); 
    it('Entering value for "begins with" logic:', function() {   
        element(by.name('text_bw')).sendKeys("developer's choice").then(function(text) {
            console.info("Value entered in text field for checking 'text_bw' functionality ");
        });
    });
    it('Checking "begins with" logic for textbox at visible point', function() {
        browser.sleep(2000);
        var bw=element(by.name('toggle_bw')); 
        bw.isDisplayed().then(function(present) {
            if (present) {
                browser.actions().mouseMove(bw).click().perform();
                console.log("element exists and test is successfully passed");
              } else {
                 console.log("Error: element doesn't exist");
              }

        });
    });
    it('Checking "not contains" logic for textbox at invisible point', function() { 
        element(by.name('toggle_nc')).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element for 'not contains' logic doesn't exist which is expected.");
              }
        });
    }); 
    it('Entering value for "not contains" logic:', function() {   
        element(by.name('text_nc')).sendKeys("nextgenpms").then(function(text) {
            console.info("Value entered in text field for checking 'not contains' functionality ");
        });
    });
    it('Checking "not contains" logic for textbox at visible point', function() {
        browser.sleep(2000);
        var bw=element(by.name('toggle_nc')); 
        bw.isDisplayed().then(function(present) {
            if (present) {
                browser.actions().mouseMove(bw).click().perform();
                console.log("element exists and test is successfully passed for 'not contains'");
              } else {
                 console.log("Error: element doesn't exist for 'not contains'");
              }

        });
    });
    it('Checking "equals to" logic for textbox at invisible point', function() { 
        element(by.name('toggle_equal')).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element for 'equals to ' logic doesn't exist which is expected.");
              }
        });
    }); 
    it('Entering value for "equals to" logic:', function() {   
        element(by.name('text_equal')).sendKeys("nextgen").then(function(text) {
            console.info("Value entered in text field for checking 'not contains' functionality ");
        });
    });

    it('Checking "equals to" logic for textbox at visible point', function() {
        browser.sleep(2000);
        var bw=element(by.name('toggle_equal')); 
        bw.isDisplayed().then(function(present) {
            if (present) {
                browser.actions().mouseMove(bw).click().perform();
                console.log("element exists and test is successfully passed for 'equals to'");
              } else {
                 console.log("Error: element doesn't exist for 'equals to'");
              }

        });
    });

    it('Checking "not equals to" logic for textbox at invisible point', function() { 
        element(by.name('toggle_ne')).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element for 'not equals to ' logic doesn't exist which is expected.");
              }
        });
    }); 
    it('Entering value for " not equals to" logic:', function() {   
        element(by.name('text_ne')).sendKeys("hello everyone").then(function(text) {
            console.info("Value entered in text field for checking 'not equals to' functionality ");
        });
    });

    it('Checking " not equals to" logic for textbox at visible point', function() {
        browser.sleep(2000);
        var bw=element(by.name('toggle_ne')); 
        bw.isDisplayed().then(function(present) {
            if (present) {
                browser.actions().mouseMove(bw).click().perform();
                console.log("element exists and test is successfully passed for 'not  equals to'");
              } else {
                 console.log("Error: element doesn't exist for 'not equals to'");
              }

        });
    });

    it('Checking "end with" logic for textbox at invisible point', function() { 
        element(by.name('toggle_ew')).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element for 'end with' logic doesn't exist which is expected.");
              }
        });
    }); 
    it('Entering value for "end with" logic:', function() {   
        element(by.name('text_ew')).sendKeys("nextgen").then(function(text) {
            console.info("Value entered in text field for checking 'end with' functionality ");
        });
    });

    it('Checking "end with" logic for textbox at visible point', function() {
        browser.sleep(2000);
        var bw=element(by.name('toggle_ew')); 
        bw.isDisplayed().then(function(present) {
            if (present) {
                browser.actions().mouseMove(bw).click().perform();
                console.log("element exists and test is successfully passed for 'end with'");
              } else {
                 console.log("Error: element doesn't exist for 'end with'");
              }

        });
    });
    it('Checking "greater than" logic for number element at invisible point', function() { 
        element(by.name('toggle_greater')).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element for 'greater than' logic doesn't exist which is expected.");
              }
        });
    }); 
    it('Entering value for "greater than" logic:', function() {   
        element(by.name('number_greater')).sendKeys(60000).then(function(text) {
            console.info("Value entered in number element for checking 'greater than' functionality ");
        });
    });

    it('Checking "greater than" logic for number element at visible point', function() {
        browser.sleep(2000);
        var bw=element(by.name('toggle_greater')); 
        bw.isDisplayed().then(function(present) {
            if (present) {
                browser.actions().mouseMove(bw).click().perform();
                console.log("element exists and test is successfully passed for 'greater than'");
              } else {
                 console.log("Error: element doesn't exist for 'greater than'");
              }

        });
    });
    it('Checking "greater than, equal to" logic for number element at invisible point', function() { 
        element(by.name('toggle_ge')).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element for 'greater than, equals to' logic doesn't exist which is expected.");
              }
        });
    }); 
    it('Entering value for "greater than, equals to" logic:', function() {   
        element(by.name('number_ge')).sendKeys(50000).then(function(text) {
            console.info("Value entered in number element for checking 'greater than, equal to' functionality ");
        });
    });

    it('Checking "greater than, equals to" logic for number element at visible point', function() {
        browser.sleep(2000);
        var bw=element(by.name('toggle_ge')); 
        bw.isDisplayed().then(function(present) {
            if (present) {
                browser.actions().mouseMove(bw).click().perform();
                console.log("element exists and test is successfully passed for 'greater than,equals to'");
              } else {
                 console.log("Error: element doesn't exist for 'greater than, equals to'");
              }

        });
    });
    it('Checking "less than" logic for number element at invisible point', function() { 
        element(by.name('toggle_less')).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element for 'less than' logic doesn't exist which is expected.");
              }
        });
    }); 
    it('Entering value for "less than" logic:', function() {   
        element(by.name('number_less')).sendKeys(50000).then(function(text) {
            console.info("Value entered in number element for checking 'less than' functionality ");
        });
    });

    it('Checking "less than" logic for number element at visible point', function() {
        browser.sleep(2000);
        var bw=element(by.name('toggle_less')); 
        bw.isDisplayed().then(function(present) {
            if (present) {
                browser.actions().mouseMove(bw).click().perform();
                console.log("element exists and test is successfully passed for 'less than'");
              } else {
                 console.log("Error: element doesn't exist for 'less than'");
              }
        });
    });
    it('Checking "equals to" logic for number element at invisible point', function() { 
        element(by.name('toggle_equals')).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element for 'equals to' logic doesn't exist which is expected.");
              }
        });
    }); 
    it('Entering value for "equals to" logic:', function() {   
        element(by.name('number_equals')).sendKeys(50000).then(function(text) {
            console.info("Value entered in number element for checking 'equals to' functionality ");
        });
    });

    it('Checking "equals to" logic for number element at visible point', function() {
        browser.sleep(2000);
        var bw=element(by.name('toggle_equals')); 
        bw.isDisplayed().then(function(present) {
            if (present) {
                browser.actions().mouseMove(bw).click().perform();
                console.log("element exists and test is successfully passed for 'equals to'");
              } else {
                 console.log("Error: element doesn't exist for 'equals to'");
              }

        });
    });
    it('Checking "less than, equals to" logic for number element at invisible point', function() { 
        element(by.name('toggle_le')).isDisplayed().then(function(present) {
            if (present) {
                console.log("Error:element exists which is not expected.");
              } else {
                 console.log("element for 'less than, equals to' logic doesn't exist which is expected.");
              }
        });
    }); 
    it('Entering value for "less than, equals to" logic:', function() {   
        element(by.name('number_le')).sendKeys(40000).then(function(text) {
            console.info("Value entered in number element for checking 'less than, equals to' functionality ");
        });
    });

    it('Checking "less than, equals to" logic for number element at visible point', function() {
        browser.sleep(2000);
        var bw=element(by.name('toggle_le')); 
        bw.isDisplayed().then(function(present) {
            if (present) {
                browser.actions().mouseMove(bw).click().perform();
                console.log("element exists and test is successfully passed for 'less than, equals to'");
              } else {
                 console.log("Error: element doesn't exist for 'less than, equals to'");
              }

        });
    });
        it("should able to go back to task page ",function(){
        element(by.xpath("//*[@id='nav']/li[1]/a")).click();
    });
});
