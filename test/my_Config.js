var CONFIG = require('./Config_file.js').config;
var JK_WEB=require('./Jenkins_Web_Copy_Para.js').config;
exports.config = {
  // The address of a running selenium server.
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['Validation_Feature_Spec.js'], //mention your specification file location
  // specs: CONFIG.PRECOMMIT,

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    browserName: 'chrome',
    platform:"OS x",
    // autoWebviewTimeout : 100000,
    // newCommandTimeout: 60,
    // getPageTimeout : 100000 
  },
  // baseUrl:'https://app.p3fy.com/',
  // enablePerformanceLogging:true,
  allScriptsTimeout: 100000,
  onPrepare: function() {
  browser.manage().window().setSize(1600, 1500);
  var jasmineReporters = require('jasmine-reporters');
  jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
    consolidateAll: true,
    savePath: '/Users/nextgen/Web_Test_Result',
    filePrefix: "p3-test-result" //CONFIG.FILE_PREFIX
  }));
  // var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
  //    jasmine.getEnv().addReporter(
  //       new Jasmine2HtmlReporter({
  //         // captureOnlyFailedSpecs: true,
  //         savePath: '/Users/nextgen/Web_Test_Result',
  //         // takeScreenShotsOnlyForFailedSpecs: false,
  //         cleanDestination: false,
  //         // fileName: 'MyReportName'
  //       })
  //     );

  }
};
