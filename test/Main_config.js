exports.config = {

    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['Validation_Feature_Spec.js'],

    capabilities: {
        browserName: 'chrome',
        platform: "OS x",
    },
    allScriptsTimeout: 100000,

    onPrepare: function() {
        browser.manage().window().setSize(1600, 1500);
        var jasmineReporters = require('jasmine-reporters');
        jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
            consolidateAll: true,
            savePath: '/Users/nextgen/Web_Test_Result',
            filePrefix: "p3-test-result" 
        }));

    }
};
