var path = require('path');
var ptor = browser,
driver = browser.driver;
var EC = protractor.ExpectedConditions;
describe('Protractor Demo App', function() {
  browser.get('https://app.p3fy.com/#/login');
  var email =element(by.model('credentials.email'));
  var password = element(by.model('credentials.password'));
  var login=element(by.buttonText('Sign In')); 
  it('should login', function() { 
    element(by.model('credentials.email')).sendKeys("dilip.rai@nextgenpms.com");
    element(by.model('credentials.password')).sendKeys(121);
    expect(email.getAttribute('value')).toEqual('dilip.rai@nextgenpms.com');
    expect(password.getAttribute('value')).toEqual('121');
   
    login.click().then(function(){ 
    expect(browser.getCurrentUrl()).toEqual('https://app.p3fy.com/#/tasks');
    console.log("Loged In Sucessfully");
    });
  }); 

    it('should navigate to data entry page', function() {
      var ele=element(by.xpath("//*[@id='pending']/div[16]/div[3]/div/div[1]/a"));
        browser.executeScript('window.scrollTo(0,1000);').then(function () {
         ele.click();
        })
      // href="#/task/24665/response
      element(by.xpath("//*[@id='content-main']/p3-loaded/div/p3-titlebar/div/div[2]/div[1]/div/a")).click();
    })
    it('we are in data entry page, should able to enter name:', function() {    
        element(by.name('name')).sendKeys('WEB: GHSS Itanagar').then(function(text) {
          console.log('Name entered sucessfully');
          browser.sleep(2000);
        });
    });
    // it('Should able to enter school type successfully :', function() {    
    //     element(by.name('school_type')).sendKeys('Higher Secondary').then(function(text) {
    //       console.log('School type entered sucessfully');
    //       browser.sleep(2000);
    //     });
    // });
    // it('Should select city:', function() {
    //       element(by.name('city')).element(by.cssContainingText('option', 'Itanagar')).click(); 
    //       element(by.name('city')).element(by.cssContainingText('option', 'Patna')).click();     
    //       console.log('city selected sucessfully');
    //   });

    // it('Should select state:', function() {
    //       browser.sleep(2000);
    //       // element(by.name('state')).element(by.cssContainingText('.ng-pristine.ng-untouched.ng-valid','Arunachal Pradesh')).click();
    //       element(by.xpath("//*[@id='content-main']/p3-loaded/div/form/fieldset/div[4]/div/div/p3-input-select/div/div/div/div/div[1]/label/input")).click();
    //       console.log('state selected sucessfully');
    //        browser.sleep(5000);
    //   });
    it('Should enter estd_date:', function() {
          element(by.name('estd_date')).sendKeys('11/10/2017');
          console.log('date entered successfully');
          browser.sleep(5000);
      });

    it('should upload an image file', function() {
      var fileToUpload = '/home/dilip/Downloads/birds.png',
          absolutePath = path.resolve(__dirname, fileToUpload);

      // $('input[type="file"]').sendKeys(absolutePath);   
      element.all(by.css('input[type="file"]')).get(0).sendKeys(absolutePath).then(function(){
        console.log("Image uploaded successfully")
      });
    });
    it('should upload a pdf file', function() {
      browser.sleep(5000); 
      var fileToUpload = '/home/dilip/Downloads/Test.pdf',
          absolutePath = path.resolve(__dirname, fileToUpload);
          browser.executeScript('window.scrollTo(0,1000);').then(function () {
            element.all(by.css('input[type="file"]')).get(1).sendKeys(absolutePath).then(function(){
            console.log("Pdf file uploaded successfully");
          });
      });
    });
    it('should click on save button and save the responses', function () {
      element(by.buttonText('Save')).click();
        browser.sleep(3000);

    });
});
 
