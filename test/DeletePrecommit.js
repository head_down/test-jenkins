var request = require('request');

describe('Verification of saved data:', function() {
	it("Should navigate to response page",function(){

	})
	it("Should navigate to question page",function(){
		element(by.xpath("//*[contains(text(),'Precommit_profile-1 - Basic Info')]")).click();
		element(by.cssContainingText(".ng-binding.ng-scope","Precommit_Automation")).click();
	})
	it("Should recheck all the entered data",function(){
		// expect(.getAttribute('value')).toEqual('');
		var textbox=element(by.name('test_textbox'));
		expect(textbox.getAttribute('value')).toEqual('WEB: Web_App_Testing');
		var selectbox=element(by.name('test_select'));
		expect(selectbox.getAttribute('value')).toEqual("36943");
		var paragraph=element(by.name('test_paragraph'));
		expect(paragraph.getAttribute('value')).toEqual("This is precommit testing for web application");
		var number=element(by.name('test_number'));
		expect(number.getAttribute('value')).toEqual("500000");
		var date=element(by.name('test_date'));
		expect(date.getAttribute('value')).toEqual("2016-06-09");
		var toggle=element(by.name('test_toggle'));
		expect(toggle.getAttribute('value')).toEqual("boolean:true");
		var name=element(by.name('name'));
		expect(name.getAttribute('value')).toEqual("Precommit_Automation");

	})
})


describe('Verification of data into database:', function() {
    var textbox,selectbox,photo,paragraph,number,file,date,toggle;
    var options = {
        method: 'GET',
        url: 'https://api.p3fy.com:443/api/taskResponses?filter='+JSON.stringify({"where":{"data.name":"Precommit_Automation"}})+'&access_token=J11KDR0ydYO5pCgDvdmE40NJ2H0t9spAqhtE9VoXLlKNluGJMwgjbApoFtWFJwFv',
        headers: {
            'Content-Type':'application/json',
            'Accept-Language': 'en-us',
            'Accept':'application/json'
        }
    };
    function callback(error, response, body) {
        if (!error && response.statusCode == 200) {
            var info = JSON.parse(body);
            console.log(info);
            textbox=info[0].data.test_textbox;
            selectbox=info[0].data.test_select;
            photo=info[0].data.test_photo;
            paragraph=info[0].data.test_paragraph;
            number=info[0].data.test_number;
            file=info[0].data.test_file;
            date=info[0].data.test_date;
            toggle=info[0].data.test_toggle;
        }
    }
    request(options, callback);

    it('should veify data in database:', function() {
      expect(textbox).toEqual("WEB: Web_App_Testing");
      // expect(selectbox).toEqual(36943);
      // expect(photo).toEqual('https://api.p3fy.com/api/containers/nextgen-objects/download/4NA78wMpoo0bFGN48Zd48KvZZAxzAfgezWKwUSOtmh667RZEsP12GZMPaWodtwCU_birds.png');
      expect(paragraph).toEqual('This is precommit testing for web application');
      expect(number).toEqual(500000);
      // expect(file).toEqual('https://api.p3fy.com/api/containers/nextgen-objects/download/GOUWZH6442YX49PNaUng8C7PDeJRVZpgTwzKpIgYLtjJft6ajarABJZpA4Rhxn38_test.pdf');
      expect(date).toEqual('2016-06-08T18:30:00.000Z');
      expect(toggle).toEqual(true);
    });
});




describe('Deleting Precommit_Automation program', function() {

	it("should able to delete precommit program", function () {
		element(by.xpath("//td[text()='Programs']")).click();
		browser.sleep(3000);
		element(by.cssContainingText('.ng-binding','Precommit_Automation')).click();
		element(by.cssContainingText('.p3-chart.p3-chart-summary','Projects')).click();
		element(by.xpath("//*[@id='tree-root']/li/div/div/div[2]/a[2]/span")).click();
		browser.switchTo().alert().accept();
		element(by.xpath("//td[text()='Programs']")).click();
		//element(by.xpath(".//*[@id='content-main']/p3-loaded/div/div/p3-list/ul/li[10]/div/div[3]/a[2]/span")).click();
		//element(by.cssContainingText('.ng-binding','Precommit_Automation')).element(by.css('.col-sm-2.col-xs-3.p3-cell-tools.glyphicon.glyphicon-remove')).click();
		element(by.xpath(".//*[@id='content-main']/p3-loaded/div/div/p3-list/ul/li[./div/div/a ='Precommit_Automation']")).element(by.css('.glyphicon.glyphicon-remove')).click();
		browser.switchTo().alert().accept();
		browser.sleep(3000);
			console.log("Precommit_Automation program is deleted successfully");
	});
    it("should able to go back to task page ",function(){
        element(by.xpath("//*[@id='nav']/li[1]/a")).click();
    });
});