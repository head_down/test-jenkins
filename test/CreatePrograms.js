
// Please delete all programs with name "Program created by Automation" to avoid any conflicts.


var locator=require("./Locators.js").config
var path = require('path');
var EC = protractor.ExpectedConditions;
describe('Program creation:', function() {

	var name=element(by.name("name"));
	it("should able to navigate to programs page", function () {
	element(by.xpath(locator.Programs)).click();
	element(by.xpath(locator.AddEntry)).click();
	browser.sleep(2000);
	});
	it("should able to close the pop-up once clicked on 'close' button", function () {
	  element(by.xpath(locator.closeButton)).click();
	  browser.sleep(2000);
	})
	// it("should able to close the pop-up clicking on “cross” icon on program page.", function () {
	//   element(by.xpath(locator.AddEntry)).click();
	//   var elm=element(by.xpath("//*[@id='newProgramDialog']/div/div/div[1]/button"));
	     // elm.click();
	// })
	it("should able to create a new program", function () {
	  element(by.xpath(locator.AddEntry)).click();
	  browser.sleep(2000);
	  name.sendKeys("Program_Auto").then(function(){
	  	element(by.buttonText("Save")).click();
	  	browser.sleep(2000);
	  })
	})
	it("should able to verify that new program is created with name 'Program_Auto'", function () {
	  var ele=element.all(by.cssContainingText('.ng-binding','Program_Auto')).get(0);
	      ele.isDisplayed().then(function(present) {
	        if (present) {
	        	console.log("present",present);
	            console.log("Program name verified successfully.");
	          } else {
	             console.log("Error: Program name not found.");
	          }
	       });
	    browser.sleep(2000);  
	})
	it("should close program creation pop-up,by clicking on cross mark", function () {
		element(by.xpath(locator.plusIcon)).click();
		element(by.xpath(locator.crossMark)).click();
	});
	it("should able to edit the created program", function () {
	  var loc=element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/p3-list/ul/li[./div/div/a ='Program_Auto']")).element(by.css('.glyphicon.glyphicon-pencil'));	
	  browser.executeScript('window.scrollTo(0,1000);').then(function () {
	  loc.click();
	  });		     
	})
	it("should able to change the entered program name", function () {
		var elm=element(by.xpath("//*[@id='profile']/form/fieldset/div/div[1]/div[2]/input"))
		elm.clear();
		elm.sendKeys("Program_Automation");
		element(by.buttonText("Save")).click();
		browser.sleep(3000);
	})
	it("should able to upload the picture for program.", function () {
		var fileToUpload = '/Users/nextgen/Downloads/Test_Pic/program.png',
         absolutePath = path.resolve(__dirname, fileToUpload);
         element.all(by.css('input[type="file"]')).get(0).sendKeys(absolutePath).then(function(){
         console.log("Photo element tested successfully")
      });
	});
	it("should able to unload the loaded picture for program.", function () {
		var elm=element(by.xpath(locator.imageUnload));
		browser.wait(EC.elementToBeClickable(elm), 30000);
        elm.click();
        browser.sleep(1000);
	});	

	it("should able to verify “program admin” email under “Users” tab.", function () {
		element(by.xpath(locator.userTabPgm)).click();
		browser.sleep(2000);
		var email=element(by.cssContainingText(".ng-binding.ng-scope","dilipkumar10cse21@gmail.com"));
		expect(email.getText()).toMatch('dilipkumar10cse21@gmail.com');
		var role=element(by.xpath("//*[@id='users']/p/p3-table/div[2]/table/tbody/tr[2]/td[3]/span"));
		expect(role.getText()).toMatch('PROGADMIN');
	})
	it("should able to close opened input box for user name by clicking on ‘x’ icon.", function () {
		element(by.xpath(locator.addUserPgm)).click();
		element(by.xpath(locator.addUserCrossMark)).click();
		browser.sleep(2000);
	})
	it("should able to add user by clicking on '+' icon under 'Users' tab as DATAENTRY role", function () {
		element(by.xpath(locator.addUserPgm)).click();
		element(by.id("emailField")).sendKeys("srirang.ranjalkar@nextgenpms.com");
		element(by.xpath(locator.userRole)).element(by.cssContainingText('option','DATAENTRY')).click();
		element(by.xpath(locator.addUserTick)).click().then(function() {
           console.log('New user added for DATAENTRY role');
         });
		browser.sleep(3000);
	})
	it("should able to give 'ProgramMonitor' role to user.", function () {
		// element(by.xpath(locator.addUserPgm)).click();
		element(by.id("emailField")).sendKeys("sai.kaushik@nextgenpms.com");
		element(by.xpath(locator.userRole)).element(by.cssContainingText('option','PROGMONITOR')).click();
		element(by.xpath(locator.addUserTick)).click().then(function() {
           console.log('New user added for program monitor role');
         });
		browser.sleep(2000);
	})
	it("should able to give “DataApprover” role to user.", function () {
		// element(by.xpath(locator.addUserPgm)).click();
		element(by.id("emailField")).sendKeys("dilip.rai@nextgenpms.com");
		element(by.xpath(locator.userRole)).element(by.cssContainingText('option','DATAAPPROVER')).click();
		element(by.xpath(locator.addUserTick)).click().then(function() {
           console.log('New user added for Data approver role');
         });
		element(by.xpath(locator.addUserCrossMark)).click();
		browser.sleep(2000);
	})
	it("should able to verify that the ‘records’ count is correct.", function () {
		expect(element(by.xpath(locator.recordsCountPgm)).getText()).toMatch(/4 records/);
	})
	it("should able to delete the added user under 'Users' tab.", function () {
		element(by.xpath("//*[@id='users']/p/p3-table/div[2]/table/tbody/tr[3]/td[4]/a/span")).click();
		browser.switchTo().alert().accept();
		browser.sleep(2000);
		element(by.xpath("//*[@id='users']/p/p3-table/div[2]/table/tbody/tr[4]/td[4]/a/span")).click();
		browser.switchTo().alert().accept();
		browser.sleep(2000);
	})
	it("should able to verify that the ‘records’ count is correct after deleting two records", function () {
		expect(element(by.xpath(locator.recordsCountPgm)).getText()).toMatch(/2 records/);
	})
	it("should able to enter a program name by clicking on “+” icon on program page.", function () {
		var elm=element(by.xpath(locator.Programs));
        browser.wait(EC.elementToBeClickable(elm), 10000);
        elm.click();
		element(by.xpath(locator.plusIcon)).click();
		element(by.xpath("//*[@id='nameField']")).sendKeys("Program_delete");
		element(by.xpath("//*[@id='content-main']/p3-loaded/div/div/p3-list/div[2]/div/form/button[1]")).click();
		browser.sleep(2000);
	});

})

describe("Deleting the created programs named 'Program_Automation' and 'program_delete",function(){
	it("should able to delete 'program_delete'", function () {
	  element(by.xpath(locator.Programs)).click();
	  var elm=element(by.xpath(".//*[@id='content-main']/p3-loaded/div/div/p3-list/ul/li[./div/div/a ='Program_delete']")).element(by.css('.glyphicon.glyphicon-remove'));	
	      elm.isPresent().then(function(present) {
            if (present) {
                elm.click().then(function(){
                  	browser.switchTo().alert().accept();	
                    console.log("program_delete is deleted successfully.");
                });
            } else {
                 console.log("program_delete doesn't exist which is expected.");
              }
        });
	  browser.sleep(2000);		     
	});
	it("should able to delete the created program", function () {
	  var elm=element(by.xpath(".//*[@id='content-main']/p3-loaded/div/div/p3-list/ul/li[./div/div/a ='Program_Automation']")).element(by.css('.glyphicon.glyphicon-remove'));	
	      elm.isPresent().then(function(present) {
            if (present) {
                elm.click().then(function(){
                  	browser.switchTo().alert().accept();	
                    console.log("Program_Automation is deleted successfully.");
                });
            } else {
                 console.log("Program_Automation doesn't exist which is expected.");
              }
        });
	  browser.sleep(2000);		     
	});
	it("should comeback to Task page", function () {
	element(by.xpath(locator.MyTasks)).click();		     
	})

});


